package com.gregor.anaya.storage.algorithm

import java.security.KeyPair


abstract class AsymmetricKey {

    abstract fun generateKey(alias: String): KeyPair?

    abstract fun getKey(alias: String): KeyPair?

    abstract fun containsAlias(alias: String): Boolean
}