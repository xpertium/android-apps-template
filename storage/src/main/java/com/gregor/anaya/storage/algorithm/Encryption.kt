package com.gregor.anaya.storage.algorithm

import android.content.Context
import android.os.Build
import java.security.Key



class Encryption(private val cipherWrapper: CipherWrapper, private val context: Context) {

    private val ALIAS = "com_bcp_core_storage"

    fun generateKey() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val symmetricKey = SymmetricKeyAes()

            if (!symmetricKey.containsAlias(ALIAS))
                symmetricKey.generateKey(ALIAS)
        } else {
            val asymmetricKey = AsymmetricKeyRsa(context)
            if (!asymmetricKey.containsAlias(ALIAS))
                asymmetricKey.generateKey(ALIAS)

            val asymmetricKeyRsaWrapAes = SymmetricKeyRsaWrapAes(asymmetricKey.getKey(ALIAS)!!, context)
            if (!asymmetricKeyRsaWrapAes.containsAlias(ALIAS))
                asymmetricKeyRsaWrapAes.generateKey(ALIAS)
        }
    }

    fun encrypt(data: String): String? {
        val secretKey: Key?

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            secretKey = SymmetricKeyAes().getKey(ALIAS)
        } else {
            val key = AsymmetricKeyRsa(context).getKey(ALIAS)
            secretKey = SymmetricKeyRsaWrapAes(key!!, context).getKey(ALIAS)
        }
        return secretKey?.let { cipherWrapper.encrypt(data, it) }
    }

    fun decrypt(data: String): String? {

        val secretKey: Key?

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            secretKey = SymmetricKeyAes().getKey(ALIAS)
        } else {
            val key = AsymmetricKeyRsa(context).getKey(ALIAS)
            secretKey = SymmetricKeyRsaWrapAes(key!!, context).getKey(ALIAS)
        }

        return secretKey?.let { cipherWrapper.decrypt(data, it) }
    }
}