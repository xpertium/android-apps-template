package com.gregor.anaya.storage.algorithm

import android.util.Base64
import java.security.MessageDigest



object Hash {

    fun sha256(text: String): String {
        val md = MessageDigest.getInstance("SHA-256")
        val toByte = text.toByteArray(Charsets.UTF_8)
        md.update(toByte, 0, toByte.size)
        return Base64.encodeToString(md.digest(), Base64.DEFAULT)
    }
}