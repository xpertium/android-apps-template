package com.gregor.anaya.storage



enum class StorageLevel(val value: Int) {
    /**
     * Constant used to save non-sensitive information. All data of this level must be deleted at the end of the session.
     */
    NON_SENSITIVE_NON_PERSIST(1),

    /**
     * Constant used to save non-sensitive information. All data of this level must be persist at the end of the session.
     */
    NON_SENSITIVE_PERSIST(2),

    /**
     * Constant used to save sensitive information. All data of this level must be deleted at the end of the session.
     */
    SENSITIVE_NON_PERSIST(3),

    /**
     * Constant used to save sensitive information. All data of this level must be persist at the end of the session.
     */
    SENSITIVE_PERSIST(4)
}