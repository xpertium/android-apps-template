package com.gregor.anaya.storage.algorithm

import android.content.Context
import android.security.KeyPairGeneratorSpec
import java.math.BigInteger
import java.security.*
import java.util.*
import javax.security.auth.x500.X500Principal



class AsymmetricKeyRsa(private val context: Context) : AsymmetricKey() {

    override fun generateKey(alias: String): KeyPair? {
        val generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore")

        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.YEAR, 20)

        val builder = KeyPairGeneratorSpec.Builder(context)
            .setAlias(alias)
            .setSerialNumber(BigInteger.ONE)
            .setSubject(X500Principal("CN=$alias CA Certificate"))
            .setStartDate(startDate.time)
            .setEndDate(endDate.time)

        generator.initialize(builder.build())
        return generator.generateKeyPair()
    }

    override fun getKey(alias: String): KeyPair? {
        val keyStore = KeyStore.getInstance("AndroidKeyStore").apply { load(null) }

        val privateKey = keyStore.getKey(alias, null) as PrivateKey?
        val publicKey = keyStore.getCertificate(alias)?.publicKey

        return if (privateKey != null && publicKey != null) {
            KeyPair(publicKey, privateKey)
        } else {
            null
        }
    }

    override fun containsAlias(alias: String): Boolean {
        val keyStore = KeyStore.getInstance("AndroidKeyStore").apply { load(null) }
        return keyStore.containsAlias(alias)
    }
}