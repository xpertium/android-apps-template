package com.gregor.anaya.storage.algorithm

import java.security.Key



abstract class SymmetricKey {

    abstract fun generateKey(alias: String): Key?

    abstract fun getKey(alias: String): Key?

    abstract fun containsAlias(alias: String): Boolean
}