package com.gregor.anaya.storage.algorithm

import android.content.Context
import java.security.Key
import java.security.KeyPair
import javax.crypto.Cipher
import javax.crypto.KeyGenerator



class SymmetricKeyRsaWrapAes(private val keyPair: KeyPair, private val context: Context) : SymmetricKey() {

    private val ALGORITHM = "AES"
    private val PREFERENCES_NAME = "SymmetricKeyRsaWrapAes"
    private val cipherWrapper =
        CipherWrapper(CipherWrapper.TRANSFORMATION_ASYMMETRIC)

    override fun generateKey(alias: String): Key? {
        val keyGenerator = KeyGenerator.getInstance(ALGORITHM)
        val generateKey = keyGenerator.generateKey()
        val wrapKey = cipherWrapper.wrapKey(generateKey, keyPair.public)

        val sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        sharedPreferences.edit().putString(alias, wrapKey).apply()

        return generateKey
    }

    override fun getKey(alias: String): Key? {
        val sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val wrapKey = sharedPreferences.getString(alias, "")!!
        return cipherWrapper.unWrapKey(wrapKey, ALGORITHM, Cipher.SECRET_KEY, keyPair.private)
    }

    override fun containsAlias(alias: String): Boolean {
        val sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return sharedPreferences.contains(alias)
    }
}