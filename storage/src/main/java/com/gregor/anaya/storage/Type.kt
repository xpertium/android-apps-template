package com.gregor.anaya.storage



enum class Type(val value: Int) {
    STRING(0),
    BOOLEAN(1),
    INT(2),
    LONG(4),
    FLOAT(5)
}