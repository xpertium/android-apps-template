package com.gregor.anaya.storage.algorithm

import android.util.Base64
import java.lang.RuntimeException
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec


class CipherWrapper(private val transformation: String) {

    private val IV_SEPARATOR = "*"
    private val cipher: Cipher by lazy { Cipher.getInstance(transformation) }

    companion object {
        var TRANSFORMATION_SYMMETRIC = "AES/GCM/NoPadding"
        var TRANSFORMATION_ASYMMETRIC = "RSA/ECB/PKCS1Padding"
    }

    fun encrypt(data: String, key: Key): String {

        if (transformation == TRANSFORMATION_ASYMMETRIC)
            throw RuntimeException("Doesn't support $TRANSFORMATION_ASYMMETRIC")

        cipher.init(Cipher.ENCRYPT_MODE, key)
        val bytes = data.toByteArray(Charsets.UTF_8)
        val ivBytes = cipher.iv
        val encryptedBytes = cipher.doFinal(bytes)
        val ivString = Base64.encodeToString(ivBytes, Base64.NO_WRAP)
        val encryptedString = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP)
        return "$ivString$IV_SEPARATOR$encryptedString"
    }

    fun decrypt(data: String, key: Key): String {

        if (transformation == TRANSFORMATION_ASYMMETRIC)
            throw RuntimeException("Doesn't support $TRANSFORMATION_ASYMMETRIC")

        val split = data.split(IV_SEPARATOR)
        val ivBytes = Base64.decode(split[0], Base64.NO_WRAP)
        val encryptedBytes = Base64.decode(split[1], Base64.NO_WRAP)
        cipher.init(Cipher.DECRYPT_MODE, key, GCMParameterSpec(128, ivBytes))
        return String(cipher.doFinal(encryptedBytes))
    }

    fun wrapKey(keyToBeWrapped: Key, keyToWrapWith: Key?): String {
        cipher.init(Cipher.WRAP_MODE, keyToWrapWith)
        val decodedData = cipher.wrap(keyToBeWrapped)
        return Base64.encodeToString(decodedData, Base64.NO_WRAP)
    }

    fun unWrapKey(wrappedKeyData: String, algorithm: String, wrappedKeyType: Int, keyToUnWrapWith: Key?): Key {
        val encryptedKeyData = Base64.decode(wrappedKeyData, Base64.NO_WRAP)
        cipher.init(Cipher.UNWRAP_MODE, keyToUnWrapWith)
        return cipher.unwrap(encryptedKeyData, algorithm, wrappedKeyType)
    }
}