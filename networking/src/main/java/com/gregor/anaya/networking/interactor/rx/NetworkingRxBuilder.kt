package com.gregor.anaya.networking.interactor.rx

import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.util.NetworkingHttpVerb
import io.reactivex.Single
import retrofit2.Response

/**
 * Middle class between NetworkingManager and NetworkingRxInteractor
 *
 * @param networkingConfiguration module configuration
 */
class NetworkingRxBuilder(
    private val networkingConfiguration: NetworkingConfiguration
) {

    /**
     * Depending to the httpVerb (NetworkingConfiguration), notify NetworkingRxInteractor
     */
    fun connect(): Single<Response<Any>> {

        return when (networkingConfiguration.httpVerb) {
            NetworkingHttpVerb.GET -> NetworkingRxInteractor.GET.execute(networkingConfiguration)
            NetworkingHttpVerb.POST -> NetworkingRxInteractor.POST.execute(networkingConfiguration)
            NetworkingHttpVerb.PUT -> NetworkingRxInteractor.PUT.execute(networkingConfiguration)
            NetworkingHttpVerb.PATCH -> NetworkingRxInteractor.PATCH.execute(networkingConfiguration)
            NetworkingHttpVerb.DELETE -> NetworkingRxInteractor.DELETE.execute(networkingConfiguration)
            else -> NetworkingRxInteractor.GET.execute(networkingConfiguration)
        }
    }
}