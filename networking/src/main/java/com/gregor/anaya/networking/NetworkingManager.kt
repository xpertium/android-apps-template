package com.gregor.anaya.networking

import com.gregor.anaya.networking.credential.SecureOkHttpClient
import com.gregor.anaya.networking.interactor.coroutine.NetworkingCoroutineBuilder
import com.gregor.anaya.networking.interactor.rx.NetworkingRxBuilder
import com.gregor.anaya.networking.model.Networking
import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.util.NetworkingHttpVerb
import com.gregor.anaya.networking.util.NetworkingType
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.LinkedHashMap
import java.util.concurrent.TimeUnit

object NetworkingManager {

    private var networking: Networking? = null

    /**
     *  Retrofit instance
     *
     * @param networkingConfiguration module configuration
     * @param apiService Retrofit needs this interface to initialize
     */
    private fun <T> createService(networkingConfiguration: NetworkingConfiguration, apiService: Class<T>): T {
        return retrofit(networkingConfiguration)!!.create(apiService)
    }

    fun retrofit(networkingConfiguration: NetworkingConfiguration): Retrofit? {
        networkingConfiguration.baseUrl?.let {
            val baseUrl = networkingConfiguration.baseUrl

            val retrofitBuilder = Retrofit.Builder()
                .baseUrl(baseUrl!!)
                .addConverterFactory(GsonConverterFactory.create())

            if (networkingConfiguration.type == NetworkingType.COROUTINES) {
                retrofitBuilder.addCallAdapterFactory(CoroutineCallAdapterFactory())
            } else {
                retrofitBuilder.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            }

            retrofitBuilder.client(getOkHttpClient(networkingConfiguration))
            return retrofitBuilder.build()
        }
        return null
    }

    /**
     * OKHttpClient instance
     *
     * @param networkingConfiguration module configuration
     * @return OkHttpClient instance
     */
    private fun getOkHttpClient(
        networkingConfiguration: NetworkingConfiguration
    ): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()

        /*
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
            */

        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        var okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

        if (networkingConfiguration.mutual!!) {
            val pwd = this.networking?.pwd
            val certificate = this.networking?.certificate

            if (pwd == null) error("PWD is required")
            if (certificate == null) error("Certificate is required")

            val secureOkHttpClient =
                SecureOkHttpClient.getSecureOkHttpClient(pwd, certificate)
                    .build()
            okHttpClientBuilder = secureOkHttpClient.newBuilder()
        }

        okHttpClientBuilder.connectTimeout(networkingConfiguration.timeout!!, TimeUnit.MINUTES)
        okHttpClientBuilder.readTimeout(networkingConfiguration.timeout!!, TimeUnit.MINUTES)

        val header = networkingConfiguration.header

        if (header != null && header.isNotEmpty()) {
            okHttpClientBuilder.addInterceptor(getInterceptor(header))
        }

        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)

        return okHttpClientBuilder.build()
    }

    /**
     * Add headers in interceptor
     *
     * @param header map
     */
    private fun getInterceptor(header: Map<String, String>?): Interceptor {

        return Interceptor { chain ->
            var request = chain.request()

            if (header != null && header.isNotEmpty()) {
                for ((key, value) in header) {
                    request = request.newBuilder().addHeader(key, value).build()
                }
            }

            val originalResponse = chain.proceed(request)
            originalResponse.newBuilder().build()
        }
    }

    /**
     * Call the builder to establish the connection with Coroutines
     *
     * @param networkingConfiguration module configuration
     * @return NetworkingCoroutineBuilder instance
     */
    fun with(
        networkingConfiguration: NetworkingConfiguration
    ): NetworkingCoroutineBuilder = NetworkingCoroutineBuilder(
        validate(
            replaceBaseConfiguration(networkingConfiguration)
        )
    )

    /**
     * Call the builder to establish the connection with RX
     *
     * @param networkingConfiguration module configuration
     * @return NetworkingRxBuilder instance
     */
    fun get(
        networkingConfiguration: NetworkingConfiguration
    ): NetworkingRxBuilder = NetworkingRxBuilder(
        validate(
            replaceBaseConfiguration(networkingConfiguration)
        )
    )

    /**
     *  Set values from NetworkingBaseConfiguration to NetworkingConfiguration and show errors
     *  of required parameters
     *
     *  Verb http or the body isn't sent, GET is default
     *  Verb http isn't sent but if the body, POST is default
     */
    private fun validate(networkingConfiguration: NetworkingConfiguration): NetworkingConfiguration {

        if (networkingConfiguration.baseUrl == null) {
            error("Url base is required")
        }

        if (networkingConfiguration.type == null) {
            error("NetworkingType is required")
        }

        if (networkingConfiguration.mutual == null) {
            networkingConfiguration.mutual = false
        }

        if (networkingConfiguration.httpVerb == null && networkingConfiguration.body == null) {
            networkingConfiguration.httpVerb = NetworkingHttpVerb.GET
        }

        if (networkingConfiguration.httpVerb == null && networkingConfiguration.body != null) {
            networkingConfiguration.httpVerb = NetworkingHttpVerb.POST
        }

        return networkingConfiguration
    }

    /**
     * Start the module configuration
     *
     * @param apiService Retrofit needs this interface to initialize
     * @param networkingConfiguration module configuration
     */
    fun <T> connect(
        apiService: Class<T>,
        networkingConfiguration: NetworkingConfiguration
    ): T {
        return createService(networkingConfiguration, apiService)
    }

    /**
     *  Set values from NetworkingBaseConfiguration to NetworkingConfiguration
     */
    private fun replaceBaseConfiguration(
        getNetworkingConfiguration: NetworkingConfiguration
    ): NetworkingConfiguration {

        val networkingConfiguration = NetworkingConfiguration.NetworkingConfigurationBuilder()

        if (networking?.networkingBaseConfiguration != null) {
            networkingConfiguration.baseUrl(networking?.networkingBaseConfiguration?.baseUrl)
                .timeout(networking?.networkingBaseConfiguration?.timeout)
                .type(networking?.networkingBaseConfiguration?.type)
                .mutual(networking?.networkingBaseConfiguration?.mutual)
                .bodyType(networking?.networkingBaseConfiguration?.bodyType)

            if (networking?.networkingBaseConfiguration?.header == null) {
                networkingConfiguration.header = networking?.networkingBaseConfiguration?.header
            } else if (!networking?.networkingBaseConfiguration?.header.isNullOrEmpty()) {
                val header = LinkedHashMap<String, String>()
                networking?.networkingBaseConfiguration?.header!!.forEach { (key, value) ->
                    header[key] = value
                }
                /*
                networkingConfiguration.header!!.forEach { (key, value) ->
                    header[key] = value
                }
                */
                networkingConfiguration.header(header)
            }
        }

        if (getNetworkingConfiguration.baseUrl != null) {
            networkingConfiguration.baseUrl(networkingConfiguration.baseUrl)
        }

        if (getNetworkingConfiguration.timeout != null) {
            networkingConfiguration.timeout(networkingConfiguration.timeout)
        }

        if (getNetworkingConfiguration.type != null) {
            networkingConfiguration.type(networkingConfiguration.type)
        }

        if (getNetworkingConfiguration.mutual != null) {
            networkingConfiguration.mutual(networkingConfiguration.mutual)
        }

        if (getNetworkingConfiguration.bodyType != null) {
            networkingConfiguration.bodyType(networkingConfiguration.bodyType)
        }

        networkingConfiguration.endpoint(getNetworkingConfiguration.endpoint)
            .httpVerb(getNetworkingConfiguration.httpVerb)
            .body(getNetworkingConfiguration.body)
            .header(getNetworkingConfiguration.header)

        return networkingConfiguration.config()
    }

    fun init(networking: Networking) {
        this.networking = networking
    }
}