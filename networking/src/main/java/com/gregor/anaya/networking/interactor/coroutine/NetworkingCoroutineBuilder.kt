package com.gregor.anaya.networking.interactor.coroutine

import com.gregor.anaya.networking.service.ResultService
import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.util.NetworkingHttpVerb

/**
 * Middle class between NetworkingManager and NetworkingCoroutineInteractor
 *
 * @param networkingConfiguration module configuration
 */
class NetworkingCoroutineBuilder(
    private val networkingConfiguration: NetworkingConfiguration
) {

    /**
     * Depending to the httpVerb (NetworkingConfiguration), notify NetworkingCoroutineInteractor
     */
    suspend fun connect(): ResultService {

        return when (networkingConfiguration.httpVerb) {
            NetworkingHttpVerb.GET -> NetworkingCoroutineInteractor.GET.execute(networkingConfiguration)
            NetworkingHttpVerb.POST -> NetworkingCoroutineInteractor.POST.execute(networkingConfiguration)
            NetworkingHttpVerb.PUT -> NetworkingCoroutineInteractor.PUT.execute(networkingConfiguration)
            NetworkingHttpVerb.PATCH -> NetworkingCoroutineInteractor.PATCH.execute(networkingConfiguration)
            NetworkingHttpVerb.DELETE -> NetworkingCoroutineInteractor.DELETE.execute(networkingConfiguration)
            else -> NetworkingCoroutineInteractor.GET.execute(networkingConfiguration)
        }
    }
}