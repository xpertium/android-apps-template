package com.gregor.anaya.networking.interactor.rx.formurlencoded

import com.gregor.anaya.networking.NetworkingManager
import com.gregor.anaya.networking.interactor.rx.RxPayload
import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.util.BodyUtil
import com.gregor.anaya.networking.util.NetworkingHttpVerb
import io.reactivex.Single
import okhttp3.HttpUrl
import retrofit2.Response

/**
 * Class with implements RxPayload and return a response or error
 */
class RxFormUrlEncodedPayload : RxPayload {

    @Throws(Throwable::class)
    override fun payload(networkingConfiguration: NetworkingConfiguration): Single<Response<Any>> {

        val response = NetworkingManager
            .connect(RxFormUrlEncodedService::class.java, networkingConfiguration)

        return when (networkingConfiguration.httpVerb) {
            NetworkingHttpVerb.GET -> response.get(
                url(
                    networkingConfiguration.baseUrl,
                    networkingConfiguration.endpoint
                )
            )
            NetworkingHttpVerb.DELETE -> response.delete(
                url(
                    networkingConfiguration.baseUrl,
                    networkingConfiguration.endpoint
                )
            )
            NetworkingHttpVerb.PATCH -> response.patch(
                url(
                    networkingConfiguration.baseUrl,
                    networkingConfiguration.endpoint
                ),
                BodyUtil.getBodyFormUrlEncoded(networkingConfiguration)
            )
            NetworkingHttpVerb.PUT -> response.put(
                url(
                    networkingConfiguration.baseUrl,
                    networkingConfiguration.endpoint
                ),
                BodyUtil.getBodyFormUrlEncoded(networkingConfiguration)
            )
            else -> response.post(
                url(
                    networkingConfiguration.baseUrl,
                    networkingConfiguration.endpoint
                ),
                BodyUtil.getBodyFormUrlEncoded(networkingConfiguration)
            )
        }
    }

    override fun showError(): String = "For FormUrlEncoded BodyType, payload HashMap is required"

    override fun url(baseUrl: String?, endpoint: String?) = HttpUrl.parse(baseUrl.plus(endpoint)).toString()
}