package com.gregor.anaya.networking.util

/**
 * Support at the moment for Json and FormUrlEncoded body
 */
enum class NetworkingBody {
    JSON,
    FORMURLENCODED
}