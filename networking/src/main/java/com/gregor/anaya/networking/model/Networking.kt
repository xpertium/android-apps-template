package com.gregor.anaya.networking.model

class Networking private constructor(
    /* certificate pwd*/
    var pwd: CharArray?,
    /* certificate in string */
    var certificate: String?,
    /* NetworkingBaseConfiguration init */
    var networkingBaseConfiguration: NetworkingBaseConfiguration?
) {

    data class NetworkingBuilder(
        var pwd: CharArray? = null,
        var certificate: String? = null,
        var networkingBaseConfiguration: NetworkingBaseConfiguration? = null
    ) {

        fun pwd(pwd: CharArray) = apply { this.pwd = pwd }
        fun certificate(certificate: String) = apply { this.certificate = certificate }
        fun networkingBaseConfiguration(networkingBaseConfiguration: NetworkingBaseConfiguration) =
            apply { this.networkingBaseConfiguration = networkingBaseConfiguration }

        fun build() = Networking(
            pwd,
            certificate,
            networkingBaseConfiguration
        )
    }
}