package com.gregor.anaya.networking.interactor.rx

import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.interactor.rx.formurlencoded.RxFormUrlEncodedPayload
import com.gregor.anaya.networking.interactor.rx.json.RxJsonPayload
import com.gregor.anaya.networking.util.NetworkingBody
import io.reactivex.Single
import retrofit2.Response

/**
 * Class that makes calls to the server
 */
enum class NetworkingRxInteractor {

    /* get http verb */
    GET {
        @Throws(Throwable::class)
        override fun execute(
            networkingConfiguration: NetworkingConfiguration
        ): Single<Response<Any>> {
            return NetworkingRxService.execute(
                payloadRxType(networkingConfiguration.bodyType!!).payload(
                    networkingConfiguration
                )
            )
        }
    },

    /* post http verb */
    POST {

        @Throws(Throwable::class)
        override fun execute(
            networkingConfiguration: NetworkingConfiguration
        ): Single<Response<Any>> {
            return NetworkingRxService.execute(
                payloadRxType(networkingConfiguration.bodyType!!).payload(
                    networkingConfiguration
                )
            )
        }
    },

    /* put http verb */
    PUT {

        @Throws(Throwable::class)
        override fun execute(
            networkingConfiguration: NetworkingConfiguration
        ): Single<Response<Any>> {
            return NetworkingRxService.execute(
                payloadRxType(networkingConfiguration.bodyType!!).payload(
                    networkingConfiguration
                )
            )
        }
    },

    /* patch http verb */
    PATCH {
        @Throws(Throwable::class)
        override fun execute(
            networkingConfiguration: NetworkingConfiguration
        ): Single<Response<Any>> {
            return NetworkingRxService.execute(
                payloadRxType(networkingConfiguration.bodyType!!).payload(
                    networkingConfiguration
                )
            )
        }
    },

    /* delete http verb */
    DELETE {

        @Throws(Throwable::class)
        override fun execute(
            networkingConfiguration: NetworkingConfiguration
        ): Single<Response<Any>> {
            return NetworkingRxService.execute(
                payloadRxType(networkingConfiguration.bodyType!!).payload(
                    networkingConfiguration
                )
            )
        }
    };

    /**
     *  Execute the call to the server
     *
     *  @param networkingConfiguration module configuration
     */
    @Throws(Throwable::class)
    internal abstract fun execute(
        networkingConfiguration: NetworkingConfiguration
    ): Single<Response<Any>>

    /**
     *  Validate bodyType
     *
     *  @param networkingConfiguration module configuration
     *  @return CoroutinePayload with Factory pattern
     */
    @Throws(Throwable::class)
    protected fun payloadRxType(bodyType: NetworkingBody): RxPayload {
        return when (bodyType) {
            NetworkingBody.JSON -> RxJsonPayload()
            NetworkingBody.FORMURLENCODED -> RxFormUrlEncodedPayload()
        }
    }
}