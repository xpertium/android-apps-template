package com.gregor.anaya.networking.util

/**
 * Support at the moment for coroutines
 */
enum class NetworkingType {
    COROUTINES,
    RX
}