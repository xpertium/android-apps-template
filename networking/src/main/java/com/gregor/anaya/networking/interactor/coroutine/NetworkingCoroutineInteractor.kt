package com.gregor.anaya.networking.interactor.coroutine

import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.service.ResultService
import com.gregor.anaya.networking.interactor.coroutine.formurlencoded.CoroutineFormUrlEncodedPayload
import com.gregor.anaya.networking.interactor.coroutine.json.CoroutineJsonPayload
import com.gregor.anaya.networking.util.NetworkingBody

/**
 * Class that makes calls to the server
 */
enum class NetworkingCoroutineInteractor {

    /* get http verb */
    GET {

        @Throws(Throwable::class)
        override suspend fun execute(networkingConfiguration: NetworkingConfiguration): ResultService {
            return NetworkingCoroutineService.execute(
                payloadCoroutineType(networkingConfiguration.bodyType!!).payload(networkingConfiguration)
            )
        }
    },

    /* post http verb */
    POST {
        @Throws(Throwable::class)
        override suspend fun execute(networkingConfiguration: NetworkingConfiguration): ResultService {
            return NetworkingCoroutineService.execute(
                payloadCoroutineType(networkingConfiguration.bodyType!!).payload(networkingConfiguration)
            )
        }
    },

    /* put http verb */
    PUT {
        @Throws(Throwable::class)
        override suspend fun execute(networkingConfiguration: NetworkingConfiguration): ResultService {
            return NetworkingCoroutineService.execute(
                payloadCoroutineType(networkingConfiguration.bodyType!!).payload(networkingConfiguration)
            )
        }
    },

    /* patch http verb */
    PATCH {
        @Throws(Throwable::class)
        override suspend fun execute(networkingConfiguration: NetworkingConfiguration): ResultService {
            return NetworkingCoroutineService.execute(
                payloadCoroutineType(networkingConfiguration.bodyType!!).payload(networkingConfiguration)
            )
        }
    },

    /* delete http verb */
    DELETE {
        @Throws(Throwable::class)
        override suspend fun execute(networkingConfiguration: NetworkingConfiguration): ResultService {
            return NetworkingCoroutineService.execute(
                payloadCoroutineType(networkingConfiguration.bodyType!!).payload(networkingConfiguration)
            )
        }
    };

    /**
     *  Execute the call to the server
     *
     *  @param networkingConfiguration module configuration
     */
    @Throws(Throwable::class)
    internal abstract suspend fun execute(networkingConfiguration: NetworkingConfiguration): ResultService

    /**
     *  Validate bodyType
     *
     *  @param networkingConfiguration module configuration
     *  @return CoroutinePayload with Factory pattern
     */
    @Throws(Throwable::class)
    protected fun payloadCoroutineType(bodyType: NetworkingBody): CoroutinePayload {
        return when (bodyType) {
            NetworkingBody.JSON -> CoroutineJsonPayload()
            NetworkingBody.FORMURLENCODED -> CoroutineFormUrlEncodedPayload()
        }
    }
}