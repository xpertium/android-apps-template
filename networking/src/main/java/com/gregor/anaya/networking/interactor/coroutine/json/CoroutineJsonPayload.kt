package com.gregor.anaya.networking.interactor.coroutine.json

import com.gregor.anaya.networking.NetworkingManager
import com.gregor.anaya.networking.interactor.coroutine.CoroutinePayload
import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.networking.util.NetworkingHttpVerb
import okhttp3.HttpUrl
import retrofit2.Response

/**
 * Class with implements CoroutinePayload and return a response or error
 */
class CoroutineJsonPayload : CoroutinePayload {

    @Throws(Throwable::class)
    override suspend fun payload(networkingConfiguration: NetworkingConfiguration): Response<Any> {
        val response = NetworkingManager
            .connect(CoroutineJsonService::class.java, networkingConfiguration)

        return when (networkingConfiguration.httpVerb) {
            NetworkingHttpVerb.GET -> response.get(
                url(networkingConfiguration.baseUrl, networkingConfiguration.endpoint)
            ).await()
            NetworkingHttpVerb.DELETE -> response.delete(
                url(networkingConfiguration.baseUrl, networkingConfiguration.endpoint)
            ).await()
            NetworkingHttpVerb.PUT -> response.put(
                url(networkingConfiguration.baseUrl, networkingConfiguration.endpoint),
                networkingConfiguration.body!!
            ).await()
            NetworkingHttpVerb.PATCH -> response.patch(
                url(networkingConfiguration.baseUrl, networkingConfiguration.endpoint),
                networkingConfiguration.body!!
            ).await()
            else -> response.post(
                url(networkingConfiguration.baseUrl, networkingConfiguration.endpoint),
                networkingConfiguration.body!!
            ).await()
        }
    }

    override fun showError(): String = "For Json BodyType, payload Json is required"

    override fun url(baseUrl: String?, endpoint: String?) = HttpUrl.parse(baseUrl.plus(endpoint)).toString()
}