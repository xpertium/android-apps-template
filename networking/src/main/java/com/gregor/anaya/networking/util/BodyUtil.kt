package com.gregor.anaya.networking.util

import com.gregor.anaya.networking.model.NetworkingConfiguration

object BodyUtil {

    fun getBodyFormUrlEncoded(networkingConfiguration: NetworkingConfiguration): HashMap<String, String> {
        val data: HashMap<String, String> = HashMap()
        if (networkingConfiguration.bodyType == NetworkingBody.FORMURLENCODED &&
            networkingConfiguration.body is HashMap<*, *>
        ) {
            val body = networkingConfiguration.body as? HashMap<*, *>

            body?.let {
                for (line in body.keys) {
                    val key = line as String
                    val value = body[line] as String
                    data[key] = value
                }
            }
        }
        return data
    }
}