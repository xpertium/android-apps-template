package com.gregor.anaya.util

import android.content.Context
import android.widget.ImageView
import com.gregor.anaya.manager.R
import com.squareup.picasso.Picasso

fun ImageView.loadUrl(url: String) {
    Picasso.get().load(url).placeholder(R.drawable.ic_not_image).error(R.drawable.ic_not_image).into(this)
}