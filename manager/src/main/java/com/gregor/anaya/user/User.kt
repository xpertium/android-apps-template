package com.gregor.anaya.user

data class User (
    var name : String,
    var lastName : String,
    var fullName : String,
    var email : String,
    var isLogin : Boolean
){
    constructor() : this(
        "","","","",false
    )
}

