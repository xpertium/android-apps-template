package com.gregor.anaya.user

import java.lang.AssertionError


class UserSingleton private constructor(inputUser : User) {
    private var _user: User? = inputUser
    val  user : User
    get() {
        if (_user == null) {
            _user = userCreation( User("","","","",false))
        } else {
            _user
        }
            return _user ?: throw AssertionError("Set to null by another thread")
    }

    init {
        userCreation(inputUser)
    }

    private fun userCreation(user : User): User {
            return user
    }

    fun addData(email : String, fullName : String) {
        _user?.email = email
        _user?.fullName = fullName
        _user?.isLogin = true
    }

    companion object : SingletonHolder<UserSingleton, User>(::UserSingleton)


}