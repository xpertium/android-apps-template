package com.gregor.anaya.app.addons

import android.content.Context
import com.mercadopago.android.px.addons.LocaleBehaviour
import java.util.*

object FakeLocaleBehaviourImpl : LocaleBehaviour {
    var localeTag = "es-MX"
    override fun attachBaseContext(context: Context): Context {
        val (language, country) = localeTag.split("-")
        return LocaleContextWrapper.wrap(context, Locale(language, country))
    }
}