package com.gregor.anaya.app

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.multidex.MultiDex
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.gregor.anaya.app.addons.FakeEscManagerBehaviourImpl
import com.gregor.anaya.app.addons.FakeLocaleBehaviourImpl
import com.gregor.anaya.cartdata.data.room.CartRoom
import com.gregor.anaya.manager.R
import com.gregor.anaya.shopdata.data.room.AppRoom
import com.mercadopago.android.px.addons.ESCManagerBehaviour
import com.mercadopago.android.px.addons.PXBehaviourConfigurer
import java.math.BigDecimal
import java.util.*


class App : Application() {

    lateinit var remoteConfig : FirebaseRemoteConfig
    var urlPromotion : String = ""
    var phone : String = ""
    var iconAlly : String = ""
    var isFetched : Boolean = false
    val PREF_NAME = "promotion_image"
    val PREF_NAME_PHONE = "phone_configuration"
    val PREF_NAME_ICON = "icon_ally"

    val PREF_NAME_DEFAULT = "default"
    val appRoom = AppRoom()
    val appRoomCart = CartRoom()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    companion object{
        val TAG = "BukuriFirebase"
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        appRoom.initRoom(this)
        appRoomCart.initRoomCart(this)
        FirebaseApp.initializeApp(this)
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        AppEventsLogger.activateApp(this)

        remoteConfig = FirebaseRemoteConfig.getInstance()
        getRemoteConfig()
        getIdToken()
        val escManagerBehaviour : ESCManagerBehaviour = FakeEscManagerBehaviourImpl()
        val builder = PXBehaviourConfigurer()
        builder.with(escManagerBehaviour).with(FakeLocaleBehaviourImpl).configure()
    }

    fun getRemoteConfig(){
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(3600)
            .build()
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    isFetched = true
                } else {
                    Toast.makeText(this, "Fallo de conexión",
                        Toast.LENGTH_SHORT).show()
                }
                getParameters()
            }
    }


    private fun getIdToken(){
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                Log.d(TAG, token.toString())
            })
    }
    private fun getParameters() {
        //TODO 1. get RemoteConfig parameters
        this.urlPromotion  = remoteConfig.getString("promotion_splash")
        this.phone = remoteConfig.getString("phone_sales")
        Log.d("CEL",this.phone)

        saveImage()
    }

    fun getFacebookAnalytics(context: Context): AppEventsLogger{
        return AppEventsLogger.newLogger(context)
    }
    fun getAnalytics(context: Context) : FirebaseAnalytics{
        firebaseAnalytics =  FirebaseAnalytics.getInstance(context)
        return firebaseAnalytics
    }

    private fun  saveImage(){
        val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val urlPreference = sharedPref.getString(PREF_NAME,PREF_NAME_DEFAULT)
        val phoneSales = sharedPref.getString(PREF_NAME_PHONE,PREF_NAME_DEFAULT)
        val icon = sharedPref.getString(PREF_NAME_ICON,PREF_NAME_DEFAULT)



        if(!icon.equals(PREF_NAME_DEFAULT)){
            val editor = sharedPref.edit()
            editor.putString(PREF_NAME_ICON,icon)
            editor.apply()
        }


        if(phoneSales.equals(PREF_NAME_DEFAULT) || !phoneSales.equals(phone)){
            val editor = sharedPref.edit()
            editor.putString(PREF_NAME_PHONE,phone)
            editor.apply()
        }


        if(urlPreference.equals(PREF_NAME_DEFAULT) || !urlPreference.equals(urlPromotion)){
            val editor = sharedPref.edit()
            editor.putString(PREF_NAME,urlPromotion)
            editor.apply()
        }
    }

    //CAMBIO GENERICO
    private fun comprar() {
        val logger = AppEventsLogger.newLogger(this)
        logger.logPurchase(BigDecimal.valueOf(4.32), Currency.getInstance("PEN"));
    }
}