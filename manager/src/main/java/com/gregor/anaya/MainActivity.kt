package com.gregor.anaya

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.auth.FirebaseAuth
import com.gregor.anaya.app.App
import com.gregor.anaya.manager.R
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.user.User
import com.gregor.anaya.user.UserSingleton
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val context = this
    private val application : App = App()
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val uuidd = UUID.randomUUID().toString()
        auth = FirebaseAuth.getInstance()
        val sharedPref: SharedPreferences = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE)
        val uuidPref = sharedPref.getString(uuidd,"none")
        if (uuidPref.equals("none")){
            application.getAnalytics(this).setUserId(uuidd)
            val editor = sharedPref.edit()
            editor.putString("uuid",uuidd)
            editor.apply()

        }else{
            application.getAnalytics(this).setUserId(uuidPref)
        }
        val logger = AppEventsLogger.newLogger(this)
        logger.logEvent("Open App");


        getPromotionImage()
        Thread {
            Thread.sleep(4000)
            startActivity(intentTo(Activity.Shop,context))
            finish()
        }.start()
    }

  private fun getPromotionImage(){
      val preference = getSharedPreferences(application.PREF_NAME,Context.MODE_PRIVATE)
      val url = preference.getString(application.PREF_NAME,application.PREF_NAME_DEFAULT)
      if (!url.equals(application.PREF_NAME_DEFAULT))
            Picasso.get().load(url).placeholder(R.drawable.bukuri).into(image_splash)
  }



    override fun onStart() {
        super.onStart()
        userLogin()

    }

    private fun userLogin(){
        val currentUser = auth.currentUser
        if(currentUser != null){
            UserSingleton.getInstance(
                User("", "",
                    currentUser.displayName.toString(),
                    currentUser.email.toString(),
                    true)
            )
        }else{ UserSingleton.getInstance(User())
        }
    }
}
