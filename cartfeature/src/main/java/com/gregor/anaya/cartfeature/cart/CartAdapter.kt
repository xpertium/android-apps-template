package com.gregor.anaya.cartfeature.cart

import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gregor.anaya.cartfeature.R
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.util.presentation.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_cart.view.*
import java.lang.StringBuilder
import java.text.DecimalFormat

class CartAdapter(val cartActivity: CartActivity,
                  private val onClickSwipe: (ProductModel) -> Unit
) : RecyclerView.Adapter<CartAdapter.CartHolder>() {

    var list : List<ProductModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartHolder =
        CartHolder(parent.inflate(R.layout.item_cart, false))

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CartHolder, position: Int) {
        val product = list[position]

        holder.itemView.menus.setOnClickListener {
            var qty : Int =  Integer.parseInt(holder.itemView.qualityProduct.text.toString())
            if (qty > 1) {
                qty--
                holder.itemView.qualityProduct.text = "$qty"
                notifyItemChanged(position)
                product.quantity = qty
                cartActivity.caculate(list)
                cartActivity.update(product)
            }
        }

        holder.itemView.plus.setOnClickListener {
            var qty : Int =  Integer.parseInt(holder.itemView.qualityProduct.text.toString())
            if (qty < 20) {
                qty++
                holder.itemView.qualityProduct.text = "$qty"
                product.quantity = qty
                cartActivity.caculate(list)
                cartActivity.update(product)
                notifyItemChanged(position)

            }
        }
        holder.bind(product)
    }


    class CartHolder(view : View) : RecyclerView.ViewHolder(view) {
        private val symbol : String = "S/."

        fun bind( product : ProductModel) {
            Picasso.get().load(product.imageList!![0]).fit().into(itemView.imageProduct)
            val decimalFormat = DecimalFormat("0.00")
            itemView.nameProduct.text = product.name
            val price = decimalFormat.format(product.retailPrice)
            if (product.promotion){
                itemView.priceProduct.text = symbol.plus(price)
                itemView.priceProduct.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.priceProductDiscount.visibility = View.VISIBLE
                itemView.priceProductDiscount.text = symbol.plus(decimalFormat.format(product.wholesalePrice))
            }else{
                itemView.priceProduct.paintFlags = itemView.nameProduct.paintFlags
                itemView.priceProduct.text = symbol.plus(price)
                itemView.priceProductDiscount.visibility = View.INVISIBLE
            }


            itemView.colorSizeProduct.text = product.colorList!![0]
            itemView.qualityProduct.text = product.quantity.toString()

        }
    }

    fun updateData(list: List<ProductModel>){
        this.list = list
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        onClickSwipe(list[position])
    }
}