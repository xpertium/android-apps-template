package com.gregor.anaya.cartfeature.cart

import android.app.Activity
import android.icu.text.SimpleDateFormat
import android.icu.util.TimeZone
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gregor.anaya.cartdata.data.common.Status
import com.gregor.anaya.cartdata.data.repository.CartDataRepository
import com.gregor.anaya.cartdata.data.room.dao.Cart
import com.gregor.anaya.cartdata.domain.data.server.ApiClient
import com.gregor.anaya.cartdata.domain.data.source.RetrofitDataSource
import com.gregor.anaya.cartdata.domain.usecase.CartUseCase
import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.domain.usecase.ProductUseCase
import com.gregor.anaya.user.User
import com.gregor.anaya.util.Util
import com.gregor.anaya.util.presentation.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class CartViewModel (private val cartUseCase: CartUseCase , private val productUseCase: ProductUseCase) : ScopedViewModel<CartNavigator>(){

    val list = MutableLiveData<List<ProductModel>>()
    val cartRepository = CartDataRepository(RetrofitDataSource(ApiClient.buildService()))
    val productModel = ProductModel()
    val listCart: LiveData<List<Cart>> = cartRepository.listCart


    init {
        initScope()
    }

    fun products(cart : List<Cart>){
        val listString =  generateString(cart)
        val products = productModel.fromJsonList(listString)
        list.postValue(products)
    }

    private fun generateString(value: List<Cart>?): List<String> {

        var array = mutableListOf<String>()
        if (value != null) {
            for (product in value){
                array.add(product.productModel)
            }
        }
        return array
    }

    fun onClickSwipe(cart : ProductModel){
        GlobalScope.launch(Dispatchers.IO) {
            cartUseCase.delete(cart)
        }
    }

    fun update(product : ProductModel){
        GlobalScope.launch(Dispatchers.IO) {
            cartUseCase.update(product)
        }
    }

    fun calculate(list : List<ProductModel>){
        var suma = 0.0
        for (i in list.indices) {
            suma += if (list[i].quantity > 1) {
                if(list[i].promotion){
                    list[i].wholesalePrice * list[i].quantity
                }else{
                    list[i].retailPrice * list[i].quantity
                }
            } else {
                if(list[i].promotion){
                    list[i].wholesalePrice
                }else{
                    list[i].retailPrice
                }
            }
        }

        val total = suma.toString()
        getNavigator()?.updatePrice(total)

    }

    fun sendWhatsApp(productModel: List<ProductModel>, totalPrice: String) {
            Util.sendWhatsApp(
                context = Activity(),
                productModel = productModel,
                totalPrice = totalPrice,
                phoneNumber = "9913035845"
            )

    }

    fun createPreferenceId(products : List<ProductModel>, user: User){

        val payer = Payer(user.fullName,"",user.email,getDate(),"")

        Log.d("payer1",payer.toString())

        val item = mutableListOf<ItemModel>()
        for (itemIterator in products){
            val itemModel = if (itemIterator.promotion){
                ItemModel(itemIterator.name!!,itemIterator.description!!,itemIterator.quantity,"PEN",itemIterator.wholesalePrice)
            }else {
                ItemModel(itemIterator.name!!,itemIterator.description!!,itemIterator.quantity,"PEN",itemIterator.retailPrice)
            }
            item.add(itemModel)
        }
        launch {
         val response=    cartUseCase.createPreference(item, payer)

            if(response.status == Status.SUCCESS){
               getNavigator()?.goToCheckout(response.data!!.id)

            }else{
                Log.d("gregor->>","no funco")
            }
        }
    }

    private fun getDate() : String{
        val tz = TimeZone.getTimeZone("UTC")
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
        df.timeZone = tz
        return df.format(Date())
    }
}