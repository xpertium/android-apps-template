package com.gregor.anaya.cartfeature.cart


interface CartNavigator {

    fun updatePrice(price : String)
    fun goToCheckout(preferenceId : String)

}