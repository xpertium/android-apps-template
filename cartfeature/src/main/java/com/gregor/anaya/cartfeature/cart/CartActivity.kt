package com.gregor.anaya.cartfeature.cart

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.gregor.anaya.app.App
import com.gregor.anaya.cartdata.data.room.dao.Cart
import com.gregor.anaya.cartfeature.R
import com.gregor.anaya.cartfeature.viewmodel.CartViewModelFactory
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.user.User
import com.gregor.anaya.user.UserSingleton
import com.gregor.anaya.util.BuildConfig
import com.gregor.anaya.util.Util
import com.mercadopago.android.px.configuration.PaymentConfiguration
import com.mercadopago.android.px.core.MercadoPagoCheckout
import com.mercadopago.android.px.core.SplitPaymentProcessor
import com.mercadopago.android.px.model.*
import com.mercadopago.android.px.model.internal.GenericPaymentDescriptor
import com.mercadopago.android.px.preferences.CheckoutPreference
import com.mercadopago.android.px.preferences.PaymentPreference
import kotlinx.android.synthetic.main.cart_activity.*
import java.text.DecimalFormat



class CartActivity : AppCompatActivity(), CartNavigator {


    private lateinit var cartAdapter : CartAdapter
    private lateinit var viewModel: CartViewModel
    private lateinit var listObserver : Observer<List<ProductModel>>
    private  var isObserver : Boolean = true
    private val PREF_NAME_PHONE = "phone_configuration"
    private val PREF_NAME_DEFAULT = "default"
    private val PREF_NAME = "promotion_image"
    private val app = App()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cart_activity)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Carrito"

    }

    override fun onResume() {
        super.onResume()


        viewModel =
                ViewModelProviders.of(this, CartViewModelFactory())
                    .get(CartViewModel::class.java)
        viewModel.setNavigator(this)
            viewModel.listCart.observe(this, Observer {listFavourite: List<Cart>? ->
                listFavourite?.let{
                    viewModel.products(it)

                }
            })

        cartAdapter = CartAdapter(this,viewModel::onClickSwipe)




        listObserver = Observer {
            initRecycler(it)
        }

        viewModel.list.observe(this, listObserver)



    }


    private fun initRecycler(list : List<ProductModel>){


        isEmptyState(list)

        if(list.isNotEmpty()){
            nested_scroll_view.visibility = View.VISIBLE
            linear_empty.visibility = View.GONE
        }else{
            nested_scroll_view.visibility = View.GONE
            linear_empty.visibility = View.VISIBLE
        }


        viewModel.calculate(list)
        productRecycler.setHasFixedSize(true)
        productRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        val gridLayoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        productRecycler.layoutManager = gridLayoutManager
        cartAdapter.list = list
        productRecycler.adapter = cartAdapter


        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = productRecycler.adapter as CartAdapter
                adapter.removeAt(viewHolder.adapterPosition)

                if (isObserver) {
                    viewModel.list.observe(this@CartActivity, Observer {
                            adapter.updateData(it)
                            viewModel.calculate(it)
                            isEmptyState(it)

                    })
                    isObserver = false
                }

            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(productRecycler)

        btnAccept.setOnClickListener {
            if (UserSingleton.getInstance(User()).user.isLogin){
                viewModel.createPreferenceId(cartAdapter.list, UserSingleton.getInstance(User()).user)
            }else{
                startActivity(intentTo(Activity.SignUp,this))
            }
        }

        btnSales.setOnClickListener {
            val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val phoneSales = sharedPref.getString(PREF_NAME_PHONE,PREF_NAME_DEFAULT)
            val bundle = Bundle()
            bundle.putString("sale", list.size.toString())
            app.getAnalytics(this).logEvent("sales",bundle)
            app.getFacebookAnalytics(this).logEvent("sales",bundle)
            Log.d("CEL-c",phoneSales.toString())
            phoneSales?.let { it1 -> Util.sendWhatsApp(this,list,totalCard.text.toString(), it1) }

        }


        viewModel.list.removeObserver(listObserver)


    }

    private fun isEmptyState(list: List<ProductModel>) {
        if(list.isNotEmpty()){
            nested_scroll_view.visibility = View.VISIBLE
            linear_empty.visibility = View.GONE
        }else{
            nested_scroll_view.visibility = View.GONE
            linear_empty.visibility = View.VISIBLE
        }
    }


    fun update(product : ProductModel){
        viewModel.update(product)
    }


    fun caculate(list: List<ProductModel>) {
        viewModel.calculate(list)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    override fun updatePrice(price: String) {
        val decimalFormat = DecimalFormat("0.00")
        totalCard.text = decimalFormat.format(price.toDouble())
    }

    override fun goToCheckout(preferenceId: String) {
        // TODO 6. MEERCAOO PAGO
        MercadoPagoCheckout.Builder(BuildConfig.API_CLIENT,preferenceId)
              .build().startPayment(this,1)
    }
}