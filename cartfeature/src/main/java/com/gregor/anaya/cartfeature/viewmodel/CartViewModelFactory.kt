package com.gregor.anaya.cartfeature.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gregor.anaya.cartdata.data.repository.CartDataRepository
import com.gregor.anaya.cartdata.domain.data.server.ApiClient
import com.gregor.anaya.cartdata.domain.data.source.RetrofitDataSource
import com.gregor.anaya.cartdata.domain.usecase.CartUseCase
import com.gregor.anaya.cartfeature.cart.CartViewModel
import com.gregor.anaya.shopdata.data.repository.ProductDataRepository
import com.gregor.anaya.shopdata.domain.usecase.ProductUseCase

class CartViewModelFactory : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(CartViewModel::class.java)) {
            val cartUseCase = CartUseCase(CartDataRepository(RetrofitDataSource(ApiClient.buildService())))
            val productUseCase = ProductUseCase(ProductDataRepository())


            CartViewModel(cartUseCase , productUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}