# App Template!

> El objetivo del app es que los usuarios encuentren productos o
> servicios de tu marca de manera directa, sencilla e interactiva.
> Mejorando la comunicación con tus clientes.

[App description](https://drive.google.com/file/d/1WGF0UowdKUioYWi_7hk1QW9WlLYg5c8p/view?usp=sharing)

TOBE 

![enter image description here](img/tobe.png)

# Funcionalidades

- Remote Config connection
- Firestore connection
- Firebase Storage
- LocalDataBase (Room)
- Firebase Authentication  (Facebook & Gmail)
- MercadoPago integration
- Marketing features
    - Cloud Messaging 
    - In-App Messaging 
    - Dynamic Links 
    - CrashLytics 
    - Analytics 



## Remote config

Configure the vendor phone & splash image: 

In App.kt getParameters()

    this.urlPromotion = remoteConfig.getString("promotion_splash")  
    this.phone = remoteConfig.getString("phone_sales")

  
## Firestore connection
In ProductServiceDataStore.kt 

    override suspend fun getAllProducts() : List<ProductModel> 

ProductModel.kt Fake Json:

    {
      "boolean": true,
      "id": "123456",
      "name": "123456",
      "description": "123456",
      "type": 123,
      "typeClothing" : 1,
      "idBrand" : "123123",
      "retailPrice" : 1232,
      "wholesalePrice" : 12312,
      "promotion" : true,
      "discount" : 1,
      "priority" : 1,
      "quantity" : 1,
      "isWishList" : false,
      "isCart" : false,
      "imageList": [
        "url",
        "url",
        "url"
      ],
      "size": [
        "S",
        "M",
        "XL"
      ],
      "colorList": [
        "Azul",
        "Amarillo",
        "Rojo"
      ],
    }
StoreServiceDataStore.kt 

    override suspend fun getAllStore(): List<StoreModel> 

**Json:**

    {
      "id": "123456",
      "name": "123456",
      "description": "123456",
      "phone" : "phone",
      "logo" : "urrLogo",
      "active" : true,
      "type": "Ropa de dama",
      "position":1
    }

In ProductServiceDataStore.kt 

    override suspend fun getProductByQuery(idBrand : String): List<ProductModel> {

## Firebase Storage

Here saving the products photos, then register products in Firestore, in parameter **imageList** does it references to storage link for downloaded with picasso library.

## Room DataBase

The favorite products & cart products is saving in Room. 

let check the classes:

    CartDataRepository.kt
    FavouriteDataRepository.kt


## Firebase Authentication  (Facebook & Gmail)

The SignUpActivity contains the authetication responsability  

Reference
https://firebase.google.com/docs/auth/android/facebook-login
https://firebase.google.com/docs/auth/android/google-signin


## MERCADO PAGO INTEGRATION

Reference: https://www.mercadopago.com.pe/developers/es/reference 

**Class CartActivity.kt**
    override fun goToCheckout(preferenceId: String) {  
        // TODO 6. MEERCAOO PAGO  
      MercadoPagoCheckout.Builder(BuildConfig.API_CLIENT,preferenceId)  
              .build().startPayment(this,1)  
    }

MARKETING FEATURES:

![enter image description here](img/marketing.png)
