package com.gregor.anaya.dynamicfeature

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.gregor.anaya.user.User
import com.gregor.anaya.user.UserSingleton
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() , View.OnClickListener{

    private val RC_SIGN_IN = 1
    lateinit var gso : GoogleSignInOptions
    private lateinit var auth: FirebaseAuth
    lateinit var mGoogleSignInClient : GoogleSignInClient
    lateinit var activity : Activity
    lateinit var  callbackManager: CallbackManager
    private var isFacebook = false
//TODO 5. Authetication with firebase (Facebook & Gmail)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        sign_in_button.setOnClickListener(this)
        facebook_button.setOnClickListener(this)
        activity = this
        auth = FirebaseAuth.getInstance()
        googleConfiguration()

    }



    fun googleConfiguration(){
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.clinet_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    fun facebookConfiguration(){
       progressBar.show(activity)
        isFacebook = true
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("email","public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                handleFacebookAccessToken(loginResult.accessToken)
            }
            override fun onCancel() {
               progressBar.hide(activity)
            }

            override fun onError(error: FacebookException) {
                progressBar.hide(activity)
            }
        })
    }


    private fun handleFacebookAccessToken(token: AccessToken) {

        val credential = FacebookAuthProvider.getCredential(token.token)
        activity.let {
            auth.signInWithCredential(credential)
                .addOnCompleteListener(it
                ) { task ->
                    if (task.isSuccessful) {

                        UserSingleton.getInstance(
                            User(
                                "",
                                "",
                                auth.currentUser?.displayName.toString(),
                                auth.currentUser?.email.toString(),
                                true)
                        ).addData(  auth.currentUser?.email.toString(),auth.currentUser?.displayName.toString())

                       finish()
                    } else {
                        Toast.makeText(this, "Es posible que te hayas registrado con Gmail",
                            Toast.LENGTH_SHORT).show()
                        progressBar.hide(activity)
                    }
                }
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.sign_in_button -> signIn()
            R.id.facebook_button -> facebookConfiguration()
        }
    }

    private fun signIn(){
        progressBar.show(activity)
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (isFacebook){
            callbackManager.onActivityResult(requestCode,resultCode,data)
        }
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)

            } catch (e: ApiException) {
                progressBar.hide(activity)
            }
        }
    }


    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)

        auth.signInWithCredential(credential)
        activity.let {
            auth.signInWithCredential(credential)
                .addOnCompleteListener(it
                ) { task ->
                    if (task.isSuccessful) {
                        UserSingleton.getInstance(
                            User(
                                "",
                                "",
                                auth.currentUser?.displayName.toString(),
                                auth.currentUser?.email.toString(),
                                true)
                        ).addData(auth.currentUser?.email.toString(), auth.currentUser?.displayName.toString())
                        finish()
                    } else {
                        progressBar.hide(activity)
                        Toast.makeText(this, "Error de conexión",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

}
