package com.gregor.anaya.shopdata.data.room

import android.content.Context
import androidx.room.Room

class AppRoom {

    companion object {
        var database: FavouriteDataBase? = null
    }



    fun initRoom(context: Context) {
        database = Room.databaseBuilder(
            context,
            FavouriteDataBase::class.java,
            "favourite-db")
            .build()
    }
}