package com.gregor.anaya.shopdata.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.gregor.anaya.model.model.product.ProductModel

@Dao
interface FavouriteDao {

    @Query("SELECT * FROM favourite")
    fun getAll() : LiveData<List<Favourite>>

    @Insert
     fun insert(vararg favourite: Favourite)

    @Query("DELETE FROM favourite")
    fun delete()

    @Delete
     fun deleteOne(vararg favourite: Favourite)

    @Update
    fun update(vararg favourite: Favourite)

    @Query("SELECT * FROM favourite WHERE favourite.id = :id LIMIT 1")
    fun getFavourite(id : String) : Favourite
}