package com.gregor.anaya.shopdata.domain.usecase

import androidx.lifecycle.LiveData
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import com.gregor.anaya.shopdata.domain.repository.FavouriteRepository

class FavouriteUseCase(private val favouriteRepository: FavouriteRepository) {


    suspend fun insert(productModel: ProductModel) = favouriteRepository.insert(getFavourite(productModel))

    suspend fun delete(productModel: ProductModel) = favouriteRepository.delete(getFavourite(productModel))


    suspend fun getAll() : LiveData<List<Favourite>> = favouriteRepository.getAll()



    private fun getFavourite(productModel: ProductModel) : Favourite{
        val favouriteString = productModel.toJson(productModel)
        return Favourite(favouriteString,productModel.id!!)
    }


}