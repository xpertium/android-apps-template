package com.gregor.anaya.shopdata.data.service

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.gregor.anaya.model.model.store.StoreModel
import com.gregor.anaya.shopdata.data.datastore.StoreDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class StoreServiceDataStore : StoreDataStore {
    private val parentJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + parentJob)
    var list = mutableListOf<StoreModel>()

    // TODO 2.1 GET STORE
    override suspend fun getAllStore(): List<StoreModel> {

        val db = FirebaseFirestore.getInstance()

        val fireStore =  db.collection("store").get()
        val query = listAssemble(fireStore)
        coroutineScope.launch(Dispatchers.IO) {

            val result = Tasks.await(query)
            val genericList = mutableListOf<StoreModel>()
            for (document in result) {

                val storeModel = document.toObject(StoreModel::class.java)
                storeModel.id = document.id
                if (storeModel.active){
                    genericList.add(storeModel)
                }
            }

            setListDocuments(genericList)

        } .start()

        while (list.size==0){
            Log.d("wait","store")
        }

                return list
        }


    private fun setListDocuments(genericList: MutableList<StoreModel>) {
        list = orderList(genericList).toMutableList()
    }

    private fun orderList(list : List<StoreModel>) : List<StoreModel> {

        return list.sortedWith(
             compareBy(String.CASE_INSENSITIVE_ORDER) { v -> v.position.toString() }
         )
    }
    private fun listAssemble(firestore: Task<QuerySnapshot>): Task<QuerySnapshot> {

        return firestore.addOnSuccessListener { result ->
            Log.d("MYTAG", result.toString())

        }
            .addOnFailureListener { exception ->
                Log.w("MYTAG", "Error getting documents.", exception)
            }
    }

}

