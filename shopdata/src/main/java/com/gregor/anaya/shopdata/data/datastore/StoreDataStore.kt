package com.gregor.anaya.shopdata.data.datastore

import com.gregor.anaya.model.model.store.StoreModel

interface StoreDataStore {
    suspend fun getAllStore():List<StoreModel>
}