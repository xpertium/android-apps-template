package com.gregor.anaya.shopdata.data.repository

import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.data.service.ProductServiceDataStore
import com.gregor.anaya.shopdata.domain.repository.ProductsRepository

class ProductDataRepository : ProductsRepository {

    override suspend fun getAllProducts() : List<ProductModel>  {
        return ProductServiceDataStore().getAllProducts()
    }

    override suspend fun getProductByQuery(idBrand : String): List<ProductModel> {
        return ProductServiceDataStore().getProductByQuery(idBrand)
    }

    override suspend fun insert(productModel: ProductModel) {

    }


}