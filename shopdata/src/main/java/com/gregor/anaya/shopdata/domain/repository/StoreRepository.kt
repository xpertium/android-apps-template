package com.gregor.anaya.shopdata.domain.repository

import com.gregor.anaya.model.model.store.StoreModel

interface StoreRepository {
    suspend fun getStore():List<StoreModel>
}