package com.gregor.anaya.shopdata.data.datastore

import com.gregor.anaya.model.model.product.ProductModel

interface ProductDataStore {
    suspend fun getAllProducts() : List<ProductModel>
    suspend fun getProductByQuery(idBrand : String) : List<ProductModel>
}