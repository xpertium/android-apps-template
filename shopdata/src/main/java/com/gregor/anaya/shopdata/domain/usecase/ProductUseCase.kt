package com.gregor.anaya.shopdata.domain.usecase

import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.model.model.store.StoreModel
import com.gregor.anaya.shopdata.domain.repository.ProductsRepository
import com.gregor.anaya.shopdata.domain.repository.StoreRepository

class ProductUseCase (private val productsRepository: ProductsRepository) {

    suspend fun getProductRepository() : List<ProductModel> = productsRepository.getAllProducts()

    suspend fun getProductByQuery(id : String) : List<ProductModel> = productsRepository.getProductByQuery(id)

}