package com.gregor.anaya.shopdata.domain.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import kotlinx.coroutines.Job

interface FavouriteRepository {
    suspend fun insert(vararg favourite : Favourite)
    suspend fun delete(vararg favourite : Favourite)
    suspend fun update(vararg favourite : Favourite)
    suspend fun deleteAll()
    suspend fun getAll() : LiveData<List<Favourite>>
    suspend fun getFavourite(vararg favourite : Favourite)
}