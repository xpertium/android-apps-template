package com.gregor.anaya.shopdata.domain.usecase

import com.gregor.anaya.model.model.store.StoreModel
import com.gregor.anaya.shopdata.domain.repository.StoreRepository

class StoreUseCase (private val storeRepository: StoreRepository) {
    suspend fun getStore():List<StoreModel> = storeRepository.getStore()
}