package com.gregor.anaya.shopdata.data.service

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.data.datastore.ProductDataStore
import kotlinx.coroutines.*
import java.util.ArrayList

class ProductServiceDataStore : ProductDataStore {


    var list = mutableListOf<ProductModel>()
    var i = 0
    private val parentJob = Job()
    // 1


    private val coroutineScope = CoroutineScope(Dispatchers.Main + parentJob)


    override suspend fun getAllProducts() : List<ProductModel> {
 //TODO 2. Firestore connection getProducts
        val db = FirebaseFirestore.getInstance()

        val fireStore =  db.collection("products").get()
        val query = listAssemble(fireStore)

        coroutineScope.launch(Dispatchers.IO) {
            val result = Tasks.await(query)
            val genericList = mutableListOf<ProductModel>()
                for (document in result) {
                    val colors = document.get("color") as ArrayList<String>
                    val images = document.get("image") as ArrayList<String>
                    val sizes = document.get("size") as ArrayList<String>
                    val productModel = document.toObject(ProductModel::class.java)
                    productModel.imageList = images
                    productModel.colorList = colors
                    productModel.size = sizes
                    productModel.id = document.id
                    productModel.quantity = 1
                    genericList.add(productModel)
                }

                setListDocuments(genericList)


        } .start()

        while (list.size==0){
            Log.d("wait","product")
        }

        return list

    }

    // TODO 2.2 GET PRODUCTOS BY STORE ID
    override suspend fun getProductByQuery(idBrand : String): List<ProductModel> {
        val db = FirebaseFirestore.getInstance()

        val fireStore =  db.collection("products_multimarca")
            .whereEqualTo("idBrand",idBrand)
            .get()
        val query = listAssemble(fireStore)
        coroutineScope.launch(Dispatchers.IO) {

            val result = Tasks.await(query)
            var genericList = mutableListOf<ProductModel>()
            for (document in result) {
                val colors = document.get("color") as ArrayList<String>
                val images = document.get("image") as ArrayList<String>
                val sizes = document.get("size") as ArrayList<String>
                val productModel = document.toObject(ProductModel::class.java)
                productModel.imageList = images
                productModel.colorList = colors
                productModel.size = sizes
                productModel.id = document.id
                productModel.quantity = 1
                genericList.add(productModel)
            }

            setListDocuments(genericList)

        } .start()

        while (list.size==0){
            Log.d("wait","getquery")
        }

        return list    }

    private fun setListDocuments(genericList: MutableList<ProductModel>) {
            list = genericList
    }


    private fun listAssemble(firestore: Task<QuerySnapshot>): Task<QuerySnapshot> {

       return firestore.addOnSuccessListener { result ->
                Log.d("MYTAG", result.toString())

       }
            .addOnFailureListener { exception ->
                Log.w("MYTAG", "Error getting documents.", exception)
            }
    }





}