package com.gregor.anaya.shopdata.domain.repository

import com.gregor.anaya.model.model.product.ProductModel

interface ProductsRepository {

    suspend fun getAllProducts() : List<ProductModel>
    suspend fun getProductByQuery(idBrand : String) : List<ProductModel>

    suspend fun insert(productModel: ProductModel)
}