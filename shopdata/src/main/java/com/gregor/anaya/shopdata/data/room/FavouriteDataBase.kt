package com.gregor.anaya.shopdata.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import com.gregor.anaya.shopdata.data.room.dao.FavouriteDao

@Database(entities = [Favourite::class], version = 2)
abstract class FavouriteDataBase : RoomDatabase(){

    abstract fun favouriteDao(): FavouriteDao


}