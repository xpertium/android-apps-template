package com.gregor.anaya.shopdata.data.repository

import android.database.sqlite.SQLiteConstraintException
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.gregor.anaya.shopdata.data.room.AppRoom
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import com.gregor.anaya.shopdata.data.room.dao.FavouriteDao
import com.gregor.anaya.shopdata.domain.repository.FavouriteRepository


class FavouriteDataRepository : FavouriteRepository {

    // TODO 3. SAVES FAVORITES
    private val favouriteDao: FavouriteDao
    val listFavourite: LiveData<List<Favourite>>

    init {
        val database = AppRoom.database
        favouriteDao = database!!.favouriteDao()
        listFavourite = favouriteDao.getAll()
    }


    override suspend fun getAll(): LiveData<List<Favourite>> {
        return favouriteDao.getAll()
    }


    override suspend fun getFavourite(vararg favourite: Favourite) {
    }


    override suspend fun insert(vararg favourite: Favourite) {
        AsyncTask.execute{
        try {
                var f = favouriteDao.getFavourite(favourite.get(0).id)
                if (f == null) {
                   favouriteDao.insert(*favourite)
                } else {
                    favouriteDao.deleteOne(*favourite)
                }
            } catch (e: SQLiteConstraintException) {
        }
    }}


    override suspend fun delete(vararg favourite: Favourite) {
        AsyncTask.execute{
            favouriteDao.deleteOne(*favourite)
        }    }

    override suspend fun deleteAll() {
        AsyncTask.execute{
            favouriteDao.delete()
        }    }

    override suspend fun update(vararg favourite: Favourite) {
        AsyncTask.execute{
            favouriteDao.update(*favourite)
        } }
}