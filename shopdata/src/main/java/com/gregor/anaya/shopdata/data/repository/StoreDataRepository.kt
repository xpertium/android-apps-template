package com.gregor.anaya.shopdata.data.repository

import com.gregor.anaya.model.model.store.StoreModel
import com.gregor.anaya.shopdata.data.service.StoreServiceDataStore
import com.gregor.anaya.shopdata.domain.repository.StoreRepository

class StoreDataRepository : StoreRepository {
    override suspend fun getStore(): List<StoreModel> {
        return StoreServiceDataStore().getAllStore()
    }
}