package com.gregor.anaya.shopdata.data.room.dao

import androidx.room.*

@Entity(tableName = "favourite")
data class Favourite(@ColumnInfo(name = "product")  var productModel: String,
                     @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = false) var id: String)

