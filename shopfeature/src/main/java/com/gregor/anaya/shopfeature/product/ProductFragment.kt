package com.gregor.anaya.shopfeature.product

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.gregor.anaya.app.App
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.shopdata.data.room.dao.Favourite

import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.shopfeature.shop.ShopActivity
import com.gregor.anaya.shopfeature.viewmodel.ProductViewModelFactory
import kotlinx.android.synthetic.main.product_fragment.*

class ProductFragment : Fragment(), ProductNavigator {

    private var productAdapter : ProductAdapter? = null
    private lateinit var listObserver : Observer<List<ProductModel>>
    private lateinit var app : App
    private lateinit var activity: android.app.Activity
    private  var cacheList : List<ProductModel> = emptyList()
    companion object {
        fun newInstance() = ProductFragment()
    }

    private lateinit var viewModel: ProductViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.product_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as ShopActivity

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        app = App()

        viewModel =
            ViewModelProviders.of(this, ProductViewModelFactory()).get(ProductViewModel::class.java)
        viewModel.setNavigator(this)

        viewModel.listFavourite.observe(this, Observer {listFavourite: List<Favourite>? ->
            listFavourite?.let{
                viewModel.favourites(it)

            }
        })

        viewModel.listFavouriteModel.observe(this, Observer {list : List<ProductModel> ?  ->
            list?.let{
                    viewModel.products(
                        it,
                        cacheList
                    )
            }
        })


         listObserver = Observer {
            initRecycler(it)
        }

        viewModel.list.observe(this, listObserver)

    }

    private fun initRecycler(list : List<ProductModel>){

            productRecycler.setHasFixedSize(true)
            val gridLayoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            productRecycler.layoutManager = gridLayoutManager
            productAdapter = ProductAdapter(
                viewModel::onClickProduct,
                viewModel::onClickFavourite,
                viewModel::onClickCart,
                context!!
            )
            productAdapter?.list = list
            cacheList = list
            productRecycler.adapter = productAdapter
            progressBar.visibility = View.GONE
            productRecycler.visibility = View.VISIBLE

        viewModel.list.removeObserver(listObserver)

    }

    override fun goToDetail(productModel: ProductModel,isFavourite : Boolean) {
        val intent: Intent = intentTo(Activity.Detail,context!!)!!
        val bundle = Bundle()
        bundle.putSerializable("product",productModel)
        bundle.putSerializable("isFavourite",isFavourite)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun updateData(position: Int, product: ProductModel) {
        productAdapter?.notifyItemChanged(position,product)
    }

    override fun tagLike(product: ProductModel) {
        app.getAnalytics(activity).logEvent(
            activity.resources.getString(R.string.like), createBundle(product,
            activity.resources.getString(R.string.like_key),
            activity.resources.getString(R.string.like_value)))

        app.getFacebookAnalytics(activity).logEvent(
            activity.resources.getString(R.string.like), createBundle(product,
            activity.resources.getString(R.string.like_key),
            activity.resources.getString(R.string.like_value)))
    }

    override fun tagCart(product: ProductModel) {
        app.getAnalytics(activity).logEvent(activity.resources.getString(R.string.cart), createBundle(product,
            activity.resources.getString(R.string.like_key),
            activity.resources.getString(R.string.like_value)))
        app.getFacebookAnalytics(activity).logEvent(activity.resources.getString(R.string.cart), createBundle(product,
            activity.resources.getString(R.string.like_key),
            activity.resources.getString(R.string.like_value)))
    }

    override fun tagProduct(product: ProductModel) {
        app.getAnalytics(activity).logEvent(activity.resources.getString(R.string.product), createBundle(product,
            activity.resources.getString(R.string.like_key),
            activity.resources.getString(R.string.like_value)))

        app.getFacebookAnalytics(activity).logEvent(
            activity.resources.getString(R.string.product), createBundle(product,
            activity.resources.getString(R.string.like_key),
            activity.resources.getString(R.string.like_value)))

    }


    private fun createBundle(product: ProductModel, key : String ,value : String) : Bundle{
        val bundle = Bundle()
        bundle.putString(key, value)
        bundle.putString(activity.resources.getString(R.string.product),product.id)
        return bundle
    }


    override fun showProgress() {
    }


}
