package com.gregor.anaya.shopfeature.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gregor.anaya.shopdata.data.repository.FavouriteDataRepository
import com.gregor.anaya.shopdata.data.repository.ProductDataRepository
import com.gregor.anaya.shopdata.data.repository.StoreDataRepository
import com.gregor.anaya.shopdata.domain.repository.StoreRepository
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.shopdata.domain.usecase.ProductUseCase
import com.gregor.anaya.shopdata.domain.usecase.StoreUseCase
import com.gregor.anaya.shopfeature.favourite.FavouriteViewModel
import com.gregor.anaya.shopfeature.store.StoreViewModel

class StoreViewModelFactory : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(StoreViewModel::class.java)) {
            val storeUseCase = StoreUseCase(StoreDataRepository())

            StoreViewModel(storeUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }


}