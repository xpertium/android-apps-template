package com.gregor.anaya.shopfeature.favourite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.data.repository.FavouriteDataRepository
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.shopfeature.product.ProductNavigator
import com.gregor.anaya.util.presentation.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FavouriteViewModel (private val favouriteUseCase: FavouriteUseCase)
    : ScopedViewModel<FavouriteNavigator>(){

    val list = MutableLiveData<List<ProductModel>>()
    val productModel = ProductModel()
    val favouriteRepository = FavouriteDataRepository()

    val listFavourite: LiveData<List<Favourite>> = favouriteRepository.listFavourite

    init {
        initScope()
    }

    fun products(listFavourite : List<Favourite>){
        val listString =  generateString(listFavourite)

        list.postValue(productModel.fromJsonList(listString))
    }

    fun onClickProduct(productModel: ProductModel,isFavourite : Boolean){
        getNavigator()?.goToDetail(productModel,true)
    }

    private fun generateString(value: List<Favourite>?): List<String> {

            var array = mutableListOf<String>()
        if (value != null) {
            for (product in value){
                array.add(product.productModel)
            }
        }
        return array
    }

    fun onClickSwipe(cart : ProductModel){
        GlobalScope.launch(Dispatchers.IO) {
            favouriteUseCase.delete(cart)
        }
    }
}
