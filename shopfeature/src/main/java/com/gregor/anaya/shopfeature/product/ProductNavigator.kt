package com.gregor.anaya.shopfeature.product

import com.gregor.anaya.model.model.product.ProductModel

interface ProductNavigator {

    fun goToDetail(productModel: ProductModel,isFavourite : Boolean)

    fun updateData(position : Int, product : ProductModel)

    fun tagLike(product: ProductModel)
    fun tagCart(product: ProductModel)
    fun tagProduct(product: ProductModel)

    fun showProgress()
}