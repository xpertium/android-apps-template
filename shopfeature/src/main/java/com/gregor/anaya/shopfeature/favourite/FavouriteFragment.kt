package com.gregor.anaya.shopfeature.favourite

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.shopdata.data.room.dao.Favourite

import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.shopfeature.viewmodel.FavouriteViewModelFactory
import com.gregor.anaya.util.Util
import com.gregor.anaya.util.presentation.diffUtil
import kotlinx.android.synthetic.main.favourite_fragment.*

class FavouriteFragment : Fragment(), FavouriteNavigator{

    private var favouriteAdapter : FavouriteAdapter?=null

    companion object {
        fun newInstance() = FavouriteFragment()
    }

    private lateinit var viewModel: FavouriteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.favourite_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, FavouriteViewModelFactory())
                .get(FavouriteViewModel::class.java)

        initRecycler()

        viewModel.setNavigator(this)

        viewModel.listFavourite.observe(this, Observer {listFavourite: List<Favourite>? ->
            listFavourite?.let{
                viewModel.products(it)
            }
        })


        viewModel.list.observe(this, Observer {
            updateData(it)
        })

        facebook.setOnClickListener { goFanPage() }
        instagram.setOnClickListener { goInstagram() }
    }

    private fun initRecycler(){
        favouriteAdapter = FavouriteAdapter(viewModel::onClickProduct, viewModel::onClickSwipe)
        recyclerViewFavorite.setHasFixedSize(true)
        recyclerViewFavorite.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val gridLayoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerViewFavorite.layoutManager = gridLayoutManager
        recyclerViewFavorite.adapter = favouriteAdapter

        val swipeHandler = object : SwipeToDeleteCallback(context!!) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerViewFavorite.adapter as FavouriteAdapter
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerViewFavorite)
    }

    private fun updateData(list : List<ProductModel>){
        if(list.isNotEmpty()){
            recyclerViewFavorite.visibility = View.VISIBLE
            empty_image.visibility = View.INVISIBLE
            empty_text.visibility = View.INVISIBLE
        }else{
            recyclerViewFavorite.visibility = View.INVISIBLE
            empty_image.visibility = View.VISIBLE
            empty_text.visibility = View.VISIBLE
        }

        favouriteAdapter?.updateData(list)
    }

    override fun goToDetail(productModel: ProductModel, isFavourite: Boolean) {
        val intent: Intent = intentTo(Activity.Detail,context!!)!!
        val bundle = Bundle()
        bundle.putSerializable("product",productModel)
        bundle.putSerializable("isFavourite",isFavourite)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun showProgress() {
    }


    fun goFanPage(){
        Util.goToSocialMedia(getString(R.string.facebook),context!!)
    }

    fun goInstagram(){
        Util.goToSocialMedia(getString(R.string.instagram),context!!)
    }

}
