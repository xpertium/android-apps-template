package com.gregor.anaya.shopfeature.favourite

import com.gregor.anaya.model.model.product.ProductModel

interface FavouriteNavigator {
    fun goToDetail(productModel: ProductModel,isFavourite : Boolean)
    fun showProgress()
}