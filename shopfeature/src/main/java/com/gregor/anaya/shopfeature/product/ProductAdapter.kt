package com.gregor.anaya.shopfeature.product

import android.app.Activity
import android.content.Context
import android.graphics.ColorFilter
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.util.presentation.inflate
import com.gregor.anaya.util.views.snackbar.CustomSnackBar
import com.gregor.anaya.util.views.threedtouch.PeekAndPop
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*
import java.text.DecimalFormat


class ProductAdapter(
    private val onClickProduct: (ProductModel,Boolean) -> Unit,
    private val onClickFavourite: (ProductModel, Boolean, Int) -> Unit,
    private val onClickCart: (ProductModel, Boolean,Int) -> Unit,
    private val context : Context
) : RecyclerView.Adapter<ProductAdapter.ProductHolder>() {

    lateinit var list : List<ProductModel>

    companion object{
        const val symbol = "S/."
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder =
            ProductHolder(parent.inflate(R.layout.item_product, false))

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val product = list[position]
        holder.bind(product)


        if (product.promotion){
            val discount = product.discount
            holder.itemView.percent.visibility = View.VISIBLE
            holder.itemView.percent.text = "$discount %"
        }else {
            holder.itemView.percent.visibility = View.INVISIBLE
        }



        //INVESTIGAR NUEVOS METODOS E INSTANCIAS
        val animation = AnimationUtils.loadAnimation(context, R.anim.alpha)
        holder.itemView.likeProduct.startAnimation(animation)
        holder.itemView.likeProduct.setColorFilter(ContextCompat.getColor(context,R.color.overlay_light_30))
        val colorFilter = holder.itemView.likeProduct.colorFilter
        holder.itemView.likeProduct.setOnClickListener {

            if (holder.itemView.likeProduct.colorFilter == colorFilter) {
                holder.itemView.likeProduct.startAnimation(animation)
                holder.itemView.likeProduct.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary))
                product.isWishList = true
                onClickFavourite(product,product.isWishList, position)
            } else {
                holder.itemView.likeProduct.startAnimation(animation)
                holder.itemView.likeProduct.setColorFilter(ContextCompat.getColor(context,R.color.overlay_light_30))
                product.isWishList = false
                onClickFavourite(product,product.isWishList, position)
            }
        }

        val animationCart = AnimationUtils.loadAnimation(context, R.anim.alpha)
        holder.itemView.addToCard.startAnimation(animationCart)
        holder.itemView.addToCard.setColorFilter(ContextCompat.getColor(context,R.color.overlay_light_30))
        val colorFilterCart = holder.itemView.likeProduct.colorFilter

        holder.itemView.addToCard.setOnClickListener {

            if (holder.itemView.addToCard.colorFilter == colorFilterCart) {
                holder.itemView.addToCard.startAnimation(animationCart)
                holder.itemView.addToCard.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary))
                product.isCart = true
                onClickCart(product,product.isCart,position)


               val customSnackBar = CustomSnackBar.Builder(context, null)
                    .layout(com.gregor.anaya.util.R.layout.custom_snackbar)
                    .duration(CustomSnackBar.LENGTH.SHORT)
                    .build(it)
                    .addTittle("Se agregó a tu carrito")
                customSnackBar.show()

            } else {
                holder.itemView.addToCard.startAnimation(animationCart)
                holder.itemView.addToCard.setColorFilter(ContextCompat.getColor(context,R.color.overlay_light_30))
                product.isCart = false
                onClickCart(product,product.isCart,position)


                val customSnackBar = CustomSnackBar.Builder(context, null)
                    .layout(com.gregor.anaya.util.R.layout.custom_snackbar)
                    .duration(CustomSnackBar.LENGTH.SHORT)
                    .build(it)
                    .addTittle("Se eliminó del carrito")
                customSnackBar.show()
            }
        }


        holder.itemView.productImage.setOnClickListener {
            onClickProduct(product,product.isWishList)
        }


        val peekAndPop = PeekAndPop.Builder(context as Activity)
            .peekLayout(R.layout.peek_view)
            .longClickViews(holder.itemView.productImage)
            .build()

        val peekView = peekAndPop.peekView
        val imageView = peekView.findViewById<ImageView>(R.id.image)
        Picasso.get().load(product.imageList!![0]).placeholder(R.drawable.bukury).centerInside()
            .fit().into(imageView)


        if (product.isWishList){
            holder.itemView.likeProduct.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary))
        }

        if (product.isCart){
            holder.itemView.addToCard.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary))
        }


    }

    class ProductHolder(view : View) : RecyclerView.ViewHolder(view) {

        fun bind( product : ProductModel) {
            val decimalFormat = DecimalFormat("0.00")
            Picasso.get().load(product.imageList!![0]).fit().into(itemView.productImage)
            itemView.nameProduct.text = product.name
            val price =  decimalFormat.format(product.retailPrice)
            if (product.promotion){
                itemView.priceProduct.text = symbol.plus(price)
                itemView.priceProduct.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.priceProductDiscount.visibility = View.VISIBLE
                itemView.priceProductDiscount.text = symbol.plus(decimalFormat.format(product.wholesalePrice))
            }else{
                itemView.priceProduct.paintFlags = itemView.nameProduct.paintFlags
                itemView.priceProduct.text = symbol.plus(price)
                itemView.priceProductDiscount.visibility = View.INVISIBLE
            }

        }
    }
}