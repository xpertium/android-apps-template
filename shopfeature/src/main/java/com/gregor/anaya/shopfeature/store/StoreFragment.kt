package com.gregor.anaya.shopfeature.store

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.gregor.anaya.app.App
import com.gregor.anaya.model.model.store.StoreModel

import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.shopfeature.detailStore.DetailStoreActivity
import com.gregor.anaya.shopfeature.shop.ShopActivity
import com.gregor.anaya.shopfeature.viewmodel.StoreViewModelFactory
import kotlinx.android.synthetic.main.product_fragment.progressBar
import kotlinx.android.synthetic.main.store_fragment.*

class StoreFragment : Fragment(), StoreNavigator {

    private var storeAdapter : StoreAdapter? = null
    private lateinit var app : App
    private lateinit var activity: android.app.Activity

    companion object {
        fun newInstance() = StoreFragment()
    }

    private lateinit var viewModel: StoreViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.store_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as ShopActivity

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        app = App()
        viewModel =
            ViewModelProviders.of(this, StoreViewModelFactory())
                .get(StoreViewModel::class.java)
        viewModel.setNavigator(this)
        viewModel.getStores()


        viewModel.list.observe(this, Observer {list : List<StoreModel> ?  ->
            list?.let{
                initRecycler(it)
            }
        })




    }

    private fun initRecycler(list : List<StoreModel>){
          storeRecycler.setHasFixedSize(true)
            val gridLayoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            storeRecycler.layoutManager = gridLayoutManager
            storeAdapter = StoreAdapter(
                viewModel::onClickStore,
                context!!
            )
            storeAdapter?.list = list
            storeRecycler.adapter = storeAdapter
            progressBar.visibility = View.GONE
            storeRecycler.visibility = View.VISIBLE

    }

    override fun goToProduct(id : String) {
        val i = Intent(activity,DetailStoreActivity::class.java)
        i.putExtra("idBrand",id)
        i.putExtra("isQuery",true)
        activity.startActivity(i)
        progressBar.visibility = View.GONE

    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }


}





