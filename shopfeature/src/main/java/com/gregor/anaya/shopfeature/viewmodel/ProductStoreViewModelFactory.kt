package com.gregor.anaya.shopfeature.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gregor.anaya.cartdata.data.repository.CartDataRepository
import com.gregor.anaya.cartdata.domain.data.server.ApiClient
import com.gregor.anaya.cartdata.domain.data.source.RetrofitDataSource
import com.gregor.anaya.cartdata.domain.usecase.CartUseCase
import com.gregor.anaya.shopdata.data.repository.FavouriteDataRepository
import com.gregor.anaya.shopdata.data.repository.ProductDataRepository
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.shopdata.domain.usecase.ProductUseCase
import com.gregor.anaya.shopfeature.detailStore.product.ProductStoreViewModel

class ProductStoreViewModelFactory : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(ProductStoreViewModel::class.java)) {
            val cartUseCase = CartUseCase(CartDataRepository(RetrofitDataSource(ApiClient.buildService())))
            val productUseCase = ProductUseCase(ProductDataRepository())
            val favouriteUseCase = FavouriteUseCase(FavouriteDataRepository())

            ProductStoreViewModel(cartUseCase,productUseCase,favouriteUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}