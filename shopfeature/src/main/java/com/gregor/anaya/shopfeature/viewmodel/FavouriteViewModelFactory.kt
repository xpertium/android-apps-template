package com.gregor.anaya.shopfeature.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gregor.anaya.shopdata.data.repository.FavouriteDataRepository
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.shopfeature.favourite.FavouriteViewModel

class FavouriteViewModelFactory : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(FavouriteViewModel::class.java)) {
            val favouriteUseCase = FavouriteUseCase(FavouriteDataRepository())

            FavouriteViewModel(favouriteUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }


}