package com.gregor.anaya.shopfeature.store

import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.model.model.store.StoreModel
import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.util.presentation.inflate
import com.gregor.anaya.util.views.snackbar.CustomSnackBar
import com.gregor.anaya.util.views.threedtouch.PeekAndPop
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.android.synthetic.main.item_store.view.*
import java.text.DecimalFormat


class StoreAdapter(
    private val onClickStore: (StoreModel) -> Unit,
    private val context : Context
) : RecyclerView.Adapter<StoreAdapter.StoreViewHolder>() {

    lateinit var list : List<StoreModel>


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder =
        StoreViewHolder(parent.inflate(R.layout.item_store, false))

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val store = list[position]
        holder.bind(store)
        Picasso.get().load(store.logo)
            .placeholder(R.drawable.bukury)
            .centerInside().fit()
            .into(holder.itemView.imageStore)

        holder.itemView.setOnClickListener {
            onClickStore(store)
        }
    }

    class StoreViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        fun bind( store : StoreModel) {
            itemView.nameStore.text =  store.type
        }
    }
}