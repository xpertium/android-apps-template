package com.gregor.anaya.shopfeature.store

import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.model.model.store.StoreModel

interface StoreNavigator {

    fun goToProduct(id : String)

    fun showProgress()
}