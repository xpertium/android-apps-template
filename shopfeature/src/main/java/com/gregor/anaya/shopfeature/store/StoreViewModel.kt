package com.gregor.anaya.shopfeature.store

import androidx.lifecycle.MutableLiveData
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.model.model.store.StoreModel
import com.gregor.anaya.shopdata.domain.usecase.StoreUseCase
import com.gregor.anaya.util.presentation.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class StoreViewModel(
    private val storeUseCase: StoreUseCase)
    : ScopedViewModel<StoreNavigator>() {

    val listFavouriteModel = MutableLiveData<List<ProductModel>>()


    val list = MutableLiveData<List<StoreModel>>()
    init {
        initScope()
    }


    fun getStores(){
        GlobalScope.launch(Dispatchers.IO) {
            val documents = storeUseCase.getStore()
            list.postValue(documents)
        }
    }



    fun onClickStore(store: StoreModel){
        getNavigator()?.showProgress()
        getNavigator()?.goToProduct(store.id.toString())

    }







}
