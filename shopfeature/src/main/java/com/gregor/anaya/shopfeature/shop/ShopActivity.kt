package com.gregor.anaya.shopfeature.shop

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import com.gregor.anaya.app.App
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.shopfeature.favourite.FavouriteFragment
import com.gregor.anaya.shopfeature.product.ProductFragment
import com.gregor.anaya.shopfeature.store.StoreFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_shop.*
import java.util.*

class ShopActivity : AppCompatActivity(){


    private val TITLE_TAB_SHOP = "Shop"
    private val TITLE_TAB_FAVOURITE = "Favoritos"
    private val TITLE_TAB_STORE = "Tiendas"

    private val PREF_NAME_ICON = "icon_ally"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val adapter = ShopViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(ProductFragment.newInstance(),TITLE_TAB_SHOP)
        adapter.addFragment(StoreFragment.newInstance(),TITLE_TAB_STORE)
        adapter.addFragment(FavouriteFragment.newInstance(),TITLE_TAB_FAVOURITE)
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)


        fab.setImageDrawable(ContextCompat.getDrawable(this,com.gregor.anaya.manager.R.drawable.bk1024_x_1024_px))
        fab.setOnClickListener {
            //go to google form
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_cart_setting, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.action_cart) {
            startActivity(intentTo(Activity.Cart,this))

        }
        return super.onOptionsItemSelected(item)
    }




}
