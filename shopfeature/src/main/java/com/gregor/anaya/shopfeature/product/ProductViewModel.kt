package com.gregor.anaya.shopfeature.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gregor.anaya.cartdata.domain.usecase.CartUseCase
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.data.repository.FavouriteDataRepository
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.shopdata.domain.usecase.ProductUseCase
import com.gregor.anaya.util.presentation.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ProductViewModel(
    private val cartUseCase: CartUseCase,
    private val productUseCase: ProductUseCase,
    private val favouriteUseCase: FavouriteUseCase)
    : ScopedViewModel<ProductNavigator>() {
    private val productModel = ProductModel()
    private val favouriteRepository = FavouriteDataRepository()
    val listFavourite: LiveData<List<Favourite>> = favouriteRepository.listFavourite

    val listFavouriteModel = MutableLiveData<List<ProductModel>>()


    val list = MutableLiveData<List<ProductModel>>()
    init {
        initScope()
    }


    fun favourites(listFavourite : List<Favourite>){
        val listString =  generateString(listFavourite)
        listFavouriteModel.postValue(productModel.fromJsonList(listString))
    }


    fun products(listFavourite : List<ProductModel>, cacheList : List<ProductModel>){
        GlobalScope.launch(Dispatchers.IO) {
            val documents: List<ProductModel> = if (cacheList.isNotEmpty()){
                cacheList
            }else{
                productUseCase.getProductRepository()
            }

            if (listFavourite.isEmpty()){
                list.postValue(documents)
            }else {
                for (i in documents.indices) {
                        for (j in listFavourite.indices){
                            if (documents[i].id == listFavourite[j].id){
                                documents[i].isWishList = true
                            }
                        }
                }
                list.postValue(documents)
            }
        }
    }


    fun onClickProduct(productModel: ProductModel,isFavourite : Boolean){
        getNavigator()?.tagProduct(productModel)
        getNavigator()?.goToDetail(productModel,isFavourite)
    }

    fun onClickFavourite(productModel: ProductModel, isFavourite : Boolean, position : Int){
        GlobalScope.launch(Dispatchers.IO) {
            productModel.isWishList = isFavourite

            if (isFavourite)
                favouriteUseCase.insert(productModel)
            else
                favouriteUseCase.delete(productModel)
        }
        getNavigator()?.tagLike(productModel)
        getNavigator()?.updateData(position,productModel)

    }


    fun onClickCart(productModel: ProductModel, isCart : Boolean, position : Int){

        GlobalScope.launch(Dispatchers.IO) {
            productModel.isCart = isCart
            if (isCart)
                    cartUseCase.insert(productModel)
            else
                cartUseCase.delete(productModel)
        }
        getNavigator()?.tagCart(productModel)
        getNavigator()?.updateData(position,productModel)
    }

    private fun generateString(value: List<Favourite>?): List<String> {

        val array = mutableListOf<String>()
        if (value != null) {
            for (product in value){
                array.add(product.productModel)
            }
        }
        return array
    }



}
