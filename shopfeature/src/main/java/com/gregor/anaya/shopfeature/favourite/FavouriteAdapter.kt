package com.gregor.anaya.shopfeature.favourite

import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopfeature.R
import com.gregor.anaya.shopfeature.product.ProductAdapter
import com.gregor.anaya.util.presentation.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_favourite.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import java.lang.StringBuilder
import java.text.DecimalFormat

class FavouriteAdapter
    (private val onClickFavourite: (ProductModel, Boolean) -> Unit,
     private val onClickSwipe: (ProductModel) -> Unit)
    : RecyclerView.Adapter<FavouriteAdapter.FavouriteHolder>() {

    var list : List<ProductModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteHolder =
        FavouriteHolder(parent.inflate(R.layout.item_favourite, false))

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: FavouriteHolder, position: Int) {

        val product = list[position]
        holder.bind(product)
        holder.itemView.setOnClickListener {onClickFavourite(product,true)}
    }


    class FavouriteHolder(view : View) : RecyclerView.ViewHolder(view) {
        var symbol : String = "S/."

        fun bind( product : ProductModel) {
            val decimalFormat = DecimalFormat("0.00")
            Picasso.get().load(product.imageList!![0]).fit().into(itemView.imageFavorite)
            itemView.nameFavorite.text = product.name
            val price =  decimalFormat.format(product.retailPrice)
            if (product.promotion){
                itemView.priceFavorite.text = symbol.plus(price)
                itemView.priceFavorite.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.priceFavoriteDiscount.visibility = View.VISIBLE
                itemView.priceFavoriteDiscount.text = symbol.plus(decimalFormat.format(product.wholesalePrice))
            }else{
                itemView.priceFavorite.paintFlags = itemView.nameFavorite.paintFlags
                itemView.priceFavorite.text = symbol.plus(price)
                itemView.priceFavoriteDiscount.visibility = View.INVISIBLE
            }
        }
    }

     fun updateData(list: List<ProductModel>){
        this.list = list
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        onClickSwipe(list[position])
    }
}