package com.gregor.anaya.detailfeature.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gregor.anaya.cartdata.data.repository.CartDataRepository
import com.gregor.anaya.cartdata.domain.data.server.ApiClient
import com.gregor.anaya.cartdata.domain.data.source.RetrofitDataSource
import com.gregor.anaya.cartdata.domain.usecase.CartUseCase
import com.gregor.anaya.detaildata.domain.usecase.DetailUseCase
import com.gregor.anaya.detailfeature.detail.DetailViewModel
import com.gregor.anaya.shopdata.data.repository.FavouriteDataRepository
import com.gregor.anaya.shopdata.data.room.dao.Favourite
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase

class DetailViewModelFactory : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            val favouriteUseCase = FavouriteUseCase(FavouriteDataRepository())
            val cartUseCase = CartUseCase(CartDataRepository(RetrofitDataSource(ApiClient.buildService())))
            DetailViewModel(favouriteUseCase,cartUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}