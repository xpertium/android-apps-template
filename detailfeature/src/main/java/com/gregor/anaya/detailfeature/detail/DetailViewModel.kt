package com.gregor.anaya.detailfeature.detail

import com.gregor.anaya.cartdata.domain.usecase.CartUseCase
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.shopdata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.util.presentation.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.StringBuilder

class DetailViewModel (private val favouriteUseCase: FavouriteUseCase, private val cartUseCase: CartUseCase) : ScopedViewModel<DetailNavigator>() {
    init {
        initScope()
    }
    fun evaluateImagesViews(product : ProductModel){
        val size = product.imageList?.size
    }
    fun insertFavourite(product: ProductModel, isFavourite : Boolean){
        GlobalScope.launch(Dispatchers.IO) {
              product.isWishList = isFavourite
              favouriteUseCase.insert(product)
              showIconFavourite(isFavourite)
              product.isWishList = isFavourite
        }
    }

    fun showIconFavourite(isFavourite : Boolean){
            if(isFavourite){
                getNavigator()!!.showIconFavourite()
            }
            else {
                getNavigator()!!.hidenIconFavourite()
            }
    }

    fun insertCart(product: ProductModel, isCart : Boolean){
        GlobalScope.launch(Dispatchers.IO) {
            product.isCart = isCart
            if (isCart)
                cartUseCase.insert(product)
            else
                cartUseCase.delete(product)
        }
    }

    fun formatter(product: ProductModel){
        val builder = StringBuilder()

        product.colorList?.let {
           for(i in it.indices ){
               builder.append(product.colorList!![i])
               if (product.colorList!!.size != i+1){
                   builder.append(" - ")
               }
           }
        }
        getNavigator()?.updateColor(builder.toString())
    }


    fun formatterSizes(list: List<String>){
        when(list.size){
            3 -> getNavigator()?.updateSize()
            2 ->  evaluateSizes(list)
            1 -> evaluateOneSize(list)
        }

    }

    fun evaluateSizes(list: List<String>){
        val arrayChar = mutableListOf<Char>()
        for (i in list.indices){
            val subString =  list[i][0]
            arrayChar.add(subString)
        }
        getNavigator()?.updateTwoSizes(arrayChar)
    }

    private fun evaluateOneSize(list: List<String>) {
        val sub= list[0][0].toString().toUpperCase()

        when(sub){
            "E" -> getNavigator()?.updateStandar()
            else -> getNavigator()?.updateOneSize(sub)
        }

    }

}