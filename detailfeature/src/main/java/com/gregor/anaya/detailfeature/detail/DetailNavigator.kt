package com.gregor.anaya.detailfeature.detail

import com.gregor.anaya.shopdata.data.room.dao.Favourite

interface DetailNavigator {

    fun updateColor(text : String)
    fun updateSize()
    fun updateTwoSizes(arrayChar: MutableList<Char>)
    fun updateStandar()
    fun updateOneSize(subString: String)
    fun showIconFavourite()
    fun hidenIconFavourite()

}