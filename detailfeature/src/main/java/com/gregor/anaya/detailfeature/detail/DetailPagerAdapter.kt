package com.gregor.anaya.detailfeature.detail

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.item_slider.view.*
import androidx.viewpager.widget.ViewPager
import com.gregor.anaya.detailfeature.R


class DetailPagerAdapter(private val context: Context) :
   PagerAdapter() {

    companion object{
        var array: List<String> = mutableListOf()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun getCount(): Int {
        return array.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {


        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_slider, null)

        view.image.scaleType   = ImageView.ScaleType.CENTER_CROP
        Picasso.get().load(array[position]).fit().into(view.image)
        container.addView(view,0)

        return view
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val viewPager = container as ViewPager
        val view = `object` as View
        viewPager.removeView(view)
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }
}