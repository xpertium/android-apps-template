package com.gregor.anaya.detailfeature.detail

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.gregor.anaya.app.App
import com.gregor.anaya.detailfeature.R
import com.gregor.anaya.detailfeature.viewmodel.DetailViewModelFactory
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.util.Util
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.item_slider.*
import kotlinx.android.synthetic.main.item_slider.view.*


class DetailActivity : AppCompatActivity() , DetailNavigator {



    lateinit var viewModel : DetailViewModel
    lateinit var product : ProductModel
    var isFavourite : Boolean = false
    val PREF_NAME_PHONE = "phone_configuration"
    val PREF_NAME_DEFAULT = "default"
    val PREF_NAME = "promotion_image"
    private val app = App()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val bundle: Bundle? = intent.extras
        product = bundle?.getSerializable("product") as ProductModel
        isFavourite = bundle.getSerializable("isFavourite") as Boolean


        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "De Compras"

        description.text = product.description

        viewModel =
            ViewModelProviders.of(this, DetailViewModelFactory())
                .get(DetailViewModel::class.java)
        viewModel.setNavigator(this)
        viewModel.evaluateImagesViews(product)
        viewModel.formatter(product)
        viewModel.formatterSizes(product.size!!)
        val adapter = DetailPagerAdapter(this)
        DetailPagerAdapter.array = product.imageList!!
        viewpager.adapter = adapter
        indicator.setupWithViewPager(viewpager, true)

        buttons()
    }

    private fun buttons(){
        bt_add_to_cart.setOnClickListener {
            product.quantity = tv_qty.text.toString().toInt()
            viewModel.insertCart(product,true)
            Toast.makeText(this,"Se agregó al carrito",Toast.LENGTH_SHORT).show()
            val bundle = Bundle()
            bundle.putString("addCart", "detail activity")
            app.getAnalytics(this).logEvent("detail",bundle)
            app.getFacebookAnalytics(this).logEvent("detail",bundle)
        }
        add_to_share.setOnClickListener {
            var _product = ArrayList<ProductModel>()
            _product.add(product)

            val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val phoneSales = sharedPref.getString(PREF_NAME_PHONE,PREF_NAME_DEFAULT)

            phoneSales?.let { it1 ->
                Util.sendWhatsApp(this,_product,product.retailPrice.toString(),
                    it1
                )

                val bundle = Bundle()
                bundle.putString("sale", "detail activity")
                app.getAnalytics(this).logEvent("sale",bundle)
                app.getFacebookAnalytics(this).logEvent("sale",bundle)
            }
        }

        fab_qty_add.setOnClickListener {
           var int : Int = tv_qty.text.toString().toInt()
           int++
           tv_qty.text = int.toString()
        }

        fab_qty_sub.setOnClickListener {

            var int : Int = tv_qty.text.toString().toInt()
            if (int >1){
                int--
            }
            tv_qty.text = int.toString()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_cart_like_2,  menu)

        if(isFavourite)
             menu.findItem(R.id.action_like).setIcon(com.gregor.anaya.manager.R.drawable.ic_heart_pink)
        else{
            menu.findItem(R.id.action_like).setIcon(com.gregor.anaya.manager.R.drawable.ic_heart)
            }

        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_view) {
            Toast.makeText(this,"compartir",Toast.LENGTH_SHORT).show()
         //  var ivImage = viewpager.let { it[it.currentItem].image }
            var ivImage = image
            val builder = VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())

           var bmpUri =  Util.getLocalBitmapUri(ivImage,this)
            if (bmpUri != null) {
                // Construct a ShareIntent with link to image
                var shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
                shareIntent.type = "image/*"
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);                // Launch sharing dialog for image
                startActivity(Intent.createChooser(shareIntent, "Share Image"))
            }
        } else if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_like) {
            if (item.title == resources.getString(com.gregor.anaya.resources.R.string.liked)) {
                val bundle = Bundle()
                bundle.putString("clickFavourite", "detail activity")
                app.getAnalytics(this).logEvent("clickFavourite",bundle)
                app.getFacebookAnalytics(this).logEvent("clickFavourite",bundle)
                viewModel.insertFavourite(product,isFavourite.not())
            }
        } else if (item.itemId == R.id.action_cart) {
            startActivity(intentTo(Activity.Cart,this))
        } else {
            Toast.makeText(applicationContext, item.title, Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun updateColor(text: String) {
            txt_colors.text = text
        }

    override fun updateSize() {
        bt_size_s.visibility = View.VISIBLE
        bt_size_m.visibility = View.VISIBLE
        bt_size_l.visibility = View.VISIBLE
    }

    override fun updateTwoSizes(arrayChar: MutableList<Char>) {
        bt_size_s.visibility = View.VISIBLE
        bt_size_m.visibility = View.VISIBLE
        bt_size_s.text = arrayChar[0].toString().toUpperCase()
        bt_size_m.text = arrayChar[1].toString().toUpperCase()
    }

    override fun updateStandar() {
        onlyStandard.visibility = View.VISIBLE
    }

    override fun updateOneSize(subString: String) {
        bt_size_s.visibility = View.VISIBLE
        bt_size_s.text = subString.toString()

    }

    override fun showIconFavourite() {
        toolbar.menu.findItem(R.id.action_like).setIcon(com.gregor.anaya.manager.R.drawable.ic_heart_pink)
        isFavourite=true

    }

    override fun hidenIconFavourite() {
        toolbar.menu.findItem(R.id.action_like).setIcon(com.gregor.anaya.manager.R.drawable.ic_heart)
        isFavourite=false
    }



}
