package com.gregor.anaya.navigator

import android.content.Context
import android.content.Intent

private const val PACKAGE_NAME = "com.gregor.anaya"

fun intentTo(intentActivity: IntentActivity ,context : Context): Intent? =
    Intent(Intent.ACTION_VIEW).setClassName("com.app.bukuri.bukuri",intentActivity.className)

interface IntentActivity {
    /**
     * The activity class name.
     */
    val className: String
}

object Activity {
    /**
     * DetailActivity
     */
    object Detail : IntentActivity {
        override val className = "$PACKAGE_NAME.detailfeature.detail.DetailActivity"
    }


    object Shop : IntentActivity {
        override val className = "$PACKAGE_NAME.shopfeature.shop.ShopActivity"
    }

    object Cart : IntentActivity {
        override val className = "$PACKAGE_NAME.cartfeature.cart.CartActivity"
    }

    object SignUp : IntentActivity {
        override val className = "$PACKAGE_NAME.dynamicfeature.SignUpActivity"
    }

}