package com.gregor.anaya.featuremovie.popular

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.gregor.anaya.favouriteddata.domain.usecase.AddFavouriteUseCase
import com.gregor.anaya.favouriteddata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.favouriteddata.domain.usecase.RemoveFavouriteUseCase
import com.gregor.anaya.featuremovie.R
import com.gregor.anaya.featuremovie.popular.PopularMovieNavigator
import com.gregor.anaya.model.model.favourite.FavouriteModel
import com.gregor.anaya.model.model.movie.MovieModel
import com.gregor.anaya.moviedata.domain.usecase.MovieUseCase
import com.gregor.anaya.util.domain.ResultType
import com.gregor.anaya.util.presentation.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import com.gregor.anaya.model.mapper.movie.MovieMapper

class PopularMovieViewModel(
    private val movieUseCase: MovieUseCase,
    private val favouriteUseCase: FavouriteUseCase,
    private val addFavouriteUseCase: AddFavouriteUseCase,
    private val removeFavouriteUseCase: RemoveFavouriteUseCase
) : ScopedViewModel<PopularMovieNavigator>() {

    private var movieModel: MovieModel? = null

    init {
        initScope()
    }

    fun movie(context: Context) {
        GlobalScope.launch(Dispatchers.Main) {
            when (val result = movieUseCase.popular()) {
                is ResultType.Success -> {
                    when (val resultFavourite = favouriteUseCase.favourite(context)) {
                        is ResultType.Success -> {

                            val list: List<FavouriteModel> = Gson().fromJson(
                                Gson().toJson(resultFavourite.value),
                                object : TypeToken<List<FavouriteModel>>() {}.type
                            )

                            Log.i("z- list", Gson().toJson(list))

                            movieModel =
                                MovieMapper.validateFavourite(result.value, list)
                            getNavigator()?.showData(movieModel!!)
                        }
                        is ResultType.Error<*, *> -> Log.i(
                            "z- error",
                            Gson().toJson(resultFavourite.value)
                        )
                    }
                }
                is ResultType.Error -> Log.i("z- error", Gson().toJson(result.value))
            }
        }
    }

    fun onClickMovie(movie: MovieModel.ResultResponseModel) {
        getNavigator()?.onClickMovie(movie)
    }

    fun onClickFavourite(movie: MovieModel.ResultResponseModel, favourite: Boolean) {
        getNavigator()?.onClickFavourite(movie, favourite)
    }

    fun saveFavourite(context: Context, favouriteModel: MovieModel.ResultResponseModel) {
        GlobalScope.launch(Dispatchers.Main) {
            addFavouriteUseCase.favourite(context, MovieMapper.transform(favouriteModel))
            getNavigator()?.showToast(R.string.message_add_favourite)
            getNavigator()?.reload()
        }
    }

    fun removeFavourite(context: Context, favouriteModel: MovieModel.ResultResponseModel) {
        GlobalScope.launch(Dispatchers.Main) {
            removeFavouriteUseCase.removeFavourite(context, MovieMapper.transform(favouriteModel))
            getNavigator()?.showToast(R.string.message_delete_favourite)
            getNavigator()?.reload()
        }
    }

    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }
}