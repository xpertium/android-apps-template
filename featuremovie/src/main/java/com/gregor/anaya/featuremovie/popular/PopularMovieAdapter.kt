package com.gregor.anaya.featuremovie.popular

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gregor.anaya.featuremovie.R
import com.gregor.anaya.model.model.movie.MovieModel
import com.gregor.anaya.util.loadUrl
import com.gregor.anaya.util.presentation.diffUtil
import com.gregor.anaya.util.presentation.inflate
import kotlinx.android.synthetic.main.item_movie.view.*

class PopularMovieAdapter(
    private val onClickMovie: (MovieModel.ResultResponseModel) -> Unit,
    private val onClickFavourite: (MovieModel.ResultResponseModel, Boolean) -> Unit
) :
    RecyclerView.Adapter<PopularMovieAdapter.PopularMovieViewHolder>() {

    var list: List<MovieModel.ResultResponseModel> by diffUtil(
        emptyList(),
        areItemsTheSame = { old, new -> old.id == new.id }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularMovieViewHolder =
        PopularMovieViewHolder(parent.inflate(R.layout.item_movie, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: PopularMovieViewHolder, position: Int) {
        val movie = list[position]
        holder.bind(movie, onClickFavourite)
        holder.itemView.setOnClickListener { onClickMovie(movie) }
    }

    class PopularMovieViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        fun bind(movie: MovieModel.ResultResponseModel, onClickFavourite: (MovieModel.ResultResponseModel, Boolean) -> Unit) {
            itemView.tvTitle.text = movie.title
            itemView.tvDescription.text = movie.overview
            itemView.ivMovie.loadUrl("https://image.tmdb.org/t/p/w185/${movie.posterPath}")

            if (movie.favourite) itemView.ivFavourite.setImageResource(com.gregor.anaya.manager.R.drawable.ic_launcher_background)
            else itemView.ivFavourite.setImageResource(com.gregor.anaya.manager.R.drawable.ic_launcher_background)

            itemView.ivFavourite.setOnClickListener {
                onClickFavourite(movie, !movie.favourite)
            }
        }
    }
}
