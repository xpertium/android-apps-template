package com.gregor.anaya.featuremovie.popular

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gregor.anaya.favouriteddata.data.repository.AddFavouriteDataRepository
import com.gregor.anaya.favouriteddata.data.repository.FavouriteDataRepository
import com.gregor.anaya.favouriteddata.data.repository.RemoveFavouriteDataRepository
import com.gregor.anaya.favouriteddata.domain.usecase.AddFavouriteUseCase
import com.gregor.anaya.favouriteddata.domain.usecase.FavouriteUseCase
import com.gregor.anaya.favouriteddata.domain.usecase.RemoveFavouriteUseCase
import com.gregor.anaya.moviedata.data.repository.MovieDataRepository
import com.gregor.anaya.moviedata.domain.usecase.MovieUseCase

class PopularMovieViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(PopularMovieViewModel::class.java)) {

            val movieUseCase = MovieUseCase(MovieDataRepository())
            val favouriteUseCase = FavouriteUseCase(FavouriteDataRepository())
            val addFavouriteUseCase =
                AddFavouriteUseCase(AddFavouriteDataRepository())
            val removeFavouriteUseCase =
                RemoveFavouriteUseCase(RemoveFavouriteDataRepository())
            PopularMovieViewModel(movieUseCase, favouriteUseCase, addFavouriteUseCase, removeFavouriteUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}