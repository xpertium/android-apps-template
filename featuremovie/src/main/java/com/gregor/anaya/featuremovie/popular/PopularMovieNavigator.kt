package com.gregor.anaya.featuremovie.popular

import com.gregor.anaya.model.model.movie.MovieModel

interface PopularMovieNavigator {
    fun showData(movieModel: MovieModel)
    fun onClickMovie(movieModel: MovieModel.ResultResponseModel)
    fun onClickFavourite(movieModel: MovieModel.ResultResponseModel, favourite: Boolean)
    fun showToast(text: Int)
    fun reload()
}