package com.gregor.anaya.featuremovie.popular

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.gregor.anaya.featuremovie.R

import kotlinx.android.synthetic.main.activity_popular_movie.*
import kotlinx.android.synthetic.main.content_popular_movie.*
import com.gregor.anaya.featuremovie.base.BaseActivity
import com.gregor.anaya.model.model.movie.MovieModel
import com.gregor.anaya.navigator.Activity
import com.gregor.anaya.navigator.intentTo
import com.gregor.anaya.util.presentation.toast

class PopularMovieActivity : BaseActivity(), PopularMovieNavigator {

    override fun layout(): Int = R.layout.activity_popular_movie
    private lateinit var popularMovieViewModel: PopularMovieViewModel
    private var popularMovieAdapter: PopularMovieAdapter? = null
    private val context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popular_movie)
        setSupportActionBar(toolbar)

        popularMovieViewModel =
            ViewModelProviders.of(this, PopularMovieViewModelFactory())
                .get(PopularMovieViewModel::class.java)
        popularMovieViewModel.setNavigator(this)

        popularMovieViewModel.movie(this)

        popularMovieAdapter =
            PopularMovieAdapter(popularMovieViewModel::onClickMovie, popularMovieViewModel::onClickFavourite)
        rvData.adapter = popularMovieAdapter
    }

    override fun showData(movieModel: MovieModel) {
        popularMovieAdapter?.list = movieModel.resultResponse!!
    }

    override fun onClickMovie(movieModel: MovieModel.ResultResponseModel) {
        val intent: Intent = intentTo(Activity.Detail,context)!!
        intent.putExtra("MOVIE_ID", movieModel.id)
        startActivity(intent)
    }

    override fun onClickFavourite(movieModel: MovieModel.ResultResponseModel, favourite: Boolean) {
        Log.i("z- onClickFav", favourite.toString())
        if (favourite) popularMovieViewModel.saveFavourite(this, movieModel)
        else popularMovieViewModel.removeFavourite(this, movieModel)
    }

    override fun showToast(text: Int) {
        toast(text)
    }

    override fun reload() {
        popularMovieViewModel.movie(this)
    }
}