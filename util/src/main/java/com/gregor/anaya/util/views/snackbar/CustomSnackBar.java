package com.gregor.anaya.util.views.snackbar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.gregor.anaya.util.R;

public class CustomSnackBar {


    private View.OnClickListener clickListener;
    private LayoutInflater layoutInflater;
    private int layout;
    private int background;
    private View contentView;
    private LENGTH duration;
    private boolean swipe;

    private Snackbar snackbar;
    private Snackbar.SnackbarLayout snackbarView;

    private CustomSnackBar(Context context, View.OnClickListener clickListener) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.duration = LENGTH.LONG;
        this.background = -1;
        this.layout = -1;
        this.swipe = true;
        this.clickListener=clickListener;
    }
    private CustomSnackBar(Context context ) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.duration = LENGTH.LONG;
        this.background = -1;
        this.layout = -1;
        this.swipe = true;
    }
    public static CustomSnackBar Builder(Context context, View.OnClickListener clickListener) {
        return new CustomSnackBar(context,clickListener);
    }
    public static CustomSnackBar Builder(Context context) {
        return new CustomSnackBar(context);
    }

    public CustomSnackBar layout(int layout) {
        this.layout = layout;
        return this;
    }

    public CustomSnackBar background(int background) {
        this.background = background;
        return this;
    }

    public CustomSnackBar duration(LENGTH duration) {
        this.duration = duration;
        return this;
    }

    public CustomSnackBar swipe(boolean swipe) {
        this.swipe = swipe;
        return this;
    }

    public CustomSnackBar build(View view) {
        if (view == null) throw new CustomSnackbarException("view can not be null");
        if (layout == -1) throw new CustomSnackbarException("layout must be setted");
        switch (duration) {
            case INDEFINITE:
                snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE);
                break;
            case SHORT:
                snackbar = Snackbar.make(view, "", Snackbar.LENGTH_SHORT);
                break;
            case LONG:
                snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG);
                break;
        }
        snackbarView = (Snackbar.SnackbarLayout) snackbar.getView();

        if (!swipe) {
            snackbarView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    snackbarView.getViewTreeObserver().removeOnPreDrawListener(this);
                    ((CoordinatorLayout.LayoutParams) snackbarView.getLayoutParams()).setBehavior(null);
                    return true;
                }
            });
        }

        snackbarView.setPadding(0, 0, 0, 0);
        if (background != -1) snackbarView.setBackgroundResource(background);
        TextView text = snackbarView.findViewById(R.id.snackbar_text);
        text.setVisibility(View.INVISIBLE);
        TextView action = snackbarView.findViewById(R.id.snackbar_action);
        action.setVisibility(View.INVISIBLE);
        contentView = layoutInflater.inflate(layout, null);
        snackbarView.addView(contentView, 0);
        return this;
    }

    public void show() {
        snackbar.show();
    }

    public boolean isShowing() {
        return snackbar != null && snackbar.isShown();
    }

    public void dismiss() {
        if (snackbar != null) snackbar.dismiss();
    }

    public View getContentView() {
        return contentView;
    }

    public enum LENGTH {
        INDEFINITE, SHORT, LONG
    }

    public class CustomSnackbarException extends RuntimeException {

        public CustomSnackbarException(String detailMessage) {
            super(detailMessage);
        }

    }

    public CustomSnackBar addTittle(String title){

        TextView txtTitle =snackbarView.findViewById(R.id.snackbar_text);
        txtTitle.setTextColor(ContextCompat.getColor(layoutInflater.getContext(), android.R.color.white));
        txtTitle.setText(title);
        return this;
    }


}
