package com.gregor.anaya.util.data

import com.gregor.anaya.util.domain.Failure

object ErrorUtil {

    fun errorHandler(error: Throwable): Failure {

        val errorException: RetrofitException =
            if (error is RetrofitException) {
                error
            } else {
                RetrofitException.retrofitException(error)
            }

        return when (errorException.kind) {
            RetrofitException.Kind.HTTP -> Failure.Http
            RetrofitException.Kind.NETWORK -> Failure.NetworkConnection
            else -> Failure.UnExpected
        }
    }
}