package com.gregor.anaya.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.FileProvider
import com.gregor.anaya.model.model.product.ProductModel
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class Util {

   companion object {
       fun goToSocialMedia(url: String, context: Context) {
           val i = Intent(Intent.ACTION_VIEW)
           i.data = Uri.parse(url)
           context.startActivity(i)
       }


       fun sendWhatsApp(context: Activity, productModel: List<ProductModel>, totalPrice: String,phoneNumber:String) {
           val uriWhatsApp = "com.whatsapp"
           val isAPKInstalled = appInstalledOrNot(context, uriWhatsApp)
           val products = StringBuilder()

           for (t in productModel.indices) {
               products.append("_cod_ :").append(t+1)
                   .append("\n").append(" _producto_ : ")
                   .append(productModel[t].name)
                   .append("\n").append(" _cantidad_ : ")
                   .append(productModel[t].quantity)
                   .append("\n").append(" _precio unitario_ : ")
                   .append(productModel[t].wholesalePrice).append("\n")
             }
           var message =  "*Nota de pedido*\n" +
                   "\n" +
                   products +
                    "\n" +
                    "*Precio total:* $totalPrice"

                   if (isAPKInstalled) {
               val packageManager = context.packageManager
               val i = Intent(Intent.ACTION_VIEW)


               try {
                   val url =
                       "https://api.whatsapp.com/send?phone=51$phoneNumber&text=$message"
                   i.setPackage(uriWhatsApp)
                   i.data = Uri.parse(url)


                   if (i.resolveActivity(packageManager) != null) {
                       context.startActivity(i)
                   }
               } catch (e: Exception) {
                   e.printStackTrace()
               }

           } else {

               Toast.makeText(context, "Whatsapp no esta instalado", Toast.LENGTH_SHORT).show()
           }
       }


       fun appInstalledOrNot(activity: Activity, uri: String): Boolean {
           val pm = activity.packageManager
           try {
               pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
               return true
           } catch (e: PackageManager.NameNotFoundException) {
           }

           return false
       }


       fun getLocalBitmapUri(imageView: ImageView , context: Context): Uri? { // Extract Bitmap from ImageView drawable
           val drawable: Drawable = imageView.getDrawable()
           var bmp: Bitmap? = null
           bmp = if (drawable is BitmapDrawable) {
               (imageView.getDrawable() as BitmapDrawable).bitmap
           } else {
               return null
           }
           // Store image to default external storage directory
           var bmpUri: Uri? = null
           try { // Use methods on Context to access package-specific directories on external storage.
// This way, you don't need to request external read/write permission.
// See https://youtu.be/5xVh-7ywKpE?t=25m25s
               val file = File(
                   context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                   "share_image_" + System.currentTimeMillis() + ".png"
               )
               val out = FileOutputStream(file)
               bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
               out.close()
               // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
               bmpUri = Uri.fromFile(file)
           } catch (e: IOException) {
               e.printStackTrace()
           }
           return bmpUri
       }

       // Method when launching drawable within Glide
       fun getBitmapFromDrawable(bmp: Bitmap,context: Context): Uri? { // Store image to default external storage directory
           var bmpUri: Uri? = null
           try { // Use methods on Context to access package-specific directories on external storage.
// This way, you don't need to request external read/write permission.
// See https://youtu.be/5xVh-7ywKpE?t=25m25s
               val file = File(
                   context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                   "share_image_" + System.currentTimeMillis() + ".png"
               )
               val out = FileOutputStream(file)
               bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
               out.close()
               // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
               bmpUri = FileProvider.getUriForFile(
                   context,
                   "com.codepath.fileprovider",
                   file
               ) // use this version for API >= 24
               // **Note:** For API < 24, you may use bmpUri = Uri.fromFile(file);
           } catch (e: IOException) {
               e.printStackTrace()
           }
           return bmpUri
       }





   }




}




