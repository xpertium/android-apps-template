package com.gregor.anaya.util.domain

import com.gregor.anaya.model.model.error.ErrorModel


sealed class Failure {
    object NetworkConnection : Failure()
    object Http : Failure()
    object UnExpected : Failure()
    data class ClientError(val errorModel: ErrorModel) : Failure()
}