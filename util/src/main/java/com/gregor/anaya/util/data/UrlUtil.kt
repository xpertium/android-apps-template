package com.gregor.anaya.util.data

object UrlUtil {

    fun replaceUrlParameter(replace: String, value: String, text: String): String = text.replace("{$replace}", value)

    fun replaceUrlParameter(replace: String, value: Int, text: String): String = text.replace("{$replace}", value.toString())
}
