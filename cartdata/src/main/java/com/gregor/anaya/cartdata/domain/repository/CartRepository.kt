package com.gregor.anaya.cartdata.domain.repository

import androidx.lifecycle.LiveData
import com.gregor.anaya.cartdata.data.common.Resource
import com.gregor.anaya.cartdata.data.room.dao.Cart
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceResponse
import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer

interface CartRepository {

    suspend fun insert(vararg cart : Cart)
    suspend fun delete(vararg cart : Cart)
    suspend fun update(vararg cart : Cart)
    suspend fun deleteAll()
    suspend fun getAll() : LiveData<List<Cart>>
    suspend fun createPreference(items : List<ItemModel>, payer: Payer) : Resource<ReferenceResponse>

}