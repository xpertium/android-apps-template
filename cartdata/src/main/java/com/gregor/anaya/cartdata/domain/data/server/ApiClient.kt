package com.gregor.anaya.cartdata.domain.data.server

import com.gregor.anaya.util.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    const val connectTimeout = 1L
    const val readTimeOut = 100L
    const val writeTimeout = 15L

    private fun addInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG)interceptor.level = HttpLoggingInterceptor.Level.BODY
            else interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    fun buildService(): Retrofit {

        val protocolList: List<Protocol> = mutableListOf(Protocol.HTTP_1_1)


        val httpClient = OkHttpClient().newBuilder().addInterceptor(addInterceptor())
             .connectTimeout(connectTimeout, TimeUnit.MINUTES)
             .readTimeout(readTimeOut, TimeUnit.MINUTES)
             .writeTimeout(writeTimeout, TimeUnit.MINUTES)
            .protocols(protocolList)
             .build()



        return Retrofit.Builder()
             .baseUrl("https://api.mercadopago.com/")
             .client(httpClient)
             .addConverterFactory(GsonConverterFactory.create())
             .build()
    }
}