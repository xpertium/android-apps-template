package com.gregor.anaya.cartdata.data.room.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart")
class Cart(@ColumnInfo(name = "product")  var productModel: String,
           @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = false) var id: String)