package com.gregor.anaya.cartdata.domain.usecase

import androidx.lifecycle.LiveData
import com.gregor.anaya.cartdata.data.common.Resource
import com.gregor.anaya.cartdata.data.room.dao.Cart
import com.gregor.anaya.cartdata.domain.repository.CartRepository
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceResponse
import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer
import com.gregor.anaya.model.model.product.ProductModel
import com.gregor.anaya.model.model.store.StoreModel

class CartUseCase (private val cartRepository: CartRepository) {


    suspend fun insert(productModel: ProductModel) {
        cartRepository.insert(getFavourite(productModel))
    }

    suspend fun delete(productModel: ProductModel) {
        cartRepository.delete(getFavourite(productModel))
    }


    private fun getFavourite(productModel: ProductModel): Cart {
        val valString = productModel.toJson(productModel)
        return Cart(valString, productModel.id!!)
    }

    suspend fun getAll(): LiveData<List<Cart>> {
        return cartRepository.getAll()
    }

    suspend fun update(productModel: ProductModel) {
        cartRepository.update(getFavourite(productModel))
    }

    suspend fun createPreference(items : List<ItemModel>, payer: Payer) : Resource<ReferenceResponse>{
     return   cartRepository.createPreference(items, payer)
    }

}

