package com.gregor.anaya.cartdata.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gregor.anaya.cartdata.data.room.dao.Cart
import com.gregor.anaya.cartdata.data.room.dao.CartDao


@Database(entities = [Cart::class], version = 2)
abstract class CartDataBase : RoomDatabase(){

    abstract fun cartDao(): CartDao

}