package com.gregor.anaya.cartdata.data.common

data class Resource<out T>(val status: Status, val data: T?, val message: String?, val errorCode :String?, val errorDescription :String? ) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null,
                errorCode = "",
                errorDescription = ""
            )
        }

        fun <T> error(msg: String, errorCode :String?,  errorDescription :String?, data: T?): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                msg,
                errorCode,
                errorDescription
            )
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(
                Status.LOADING,
                data,
                null,
                errorCode = "",
                errorDescription = ""
            )
        }
    }
}
