package com.gregor.anaya.cartdata.data.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
