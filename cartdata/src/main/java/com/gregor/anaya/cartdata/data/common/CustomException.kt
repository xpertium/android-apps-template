package com.gregor.anaya.cartdata.data.common

import retrofit2.HttpException

data class CustomException(  val errorCode : String,
                             val errorDescription : String,
                             val status : String,
                             val timestamp : String) : Exception()