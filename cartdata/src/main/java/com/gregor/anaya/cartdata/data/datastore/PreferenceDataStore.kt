package com.gregor.anaya.cartdata.data.datastore

import com.gregor.anaya.cartdata.data.common.Resource
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceResponse
import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer


interface PreferenceDataStore {
    suspend fun createPreference(items : List<ItemModel>, payer: Payer)  : Resource<ReferenceResponse>
}