package com.gregor.anaya.cartdata.domain.data.source
import com.gregor.anaya.cartdata.data.common.Resource
import com.gregor.anaya.cartdata.data.datastore.PreferenceDataStore
import com.gregor.anaya.cartdata.domain.data.ResponseHandler
import com.gregor.anaya.cartdata.domain.data.server.UserService
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceRequest
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceResponse
import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer
import com.gregor.anaya.model.model.mecadopago.paymet.ExcludePaymentTypeModel
import com.gregor.anaya.model.model.mecadopago.paymet.PaymentMethodsModel
import com.gregor.anaya.util.BuildConfig
import retrofit2.Retrofit


class RetrofitDataSource(retrofit: Retrofit) : PreferenceDataStore {

    private val api = retrofit.run { create(UserService::class.java) }


    override suspend fun createPreference(items: List<ItemModel>, payer: Payer): Resource<ReferenceResponse> {
        return try {
            val response = ReferenceRequest(items,payer,excludePaymentMethods())
            val result = api.createPreference(BuildConfig.TOKEN,response)
            ResponseHandler().handleSuccess(result)
        }catch (e: Exception) {
            ResponseHandler().handleException(e)
        }
    }

    private fun excludePaymentMethods() :  PaymentMethodsModel{
        val excludeTicket =  ExcludePaymentTypeModel("ticket")
        val excludeAtm =  ExcludePaymentTypeModel("atm")
        val excludePrepaid =  ExcludePaymentTypeModel("prepaid_card")
        val listPaymentMethods = listOf(excludeAtm,excludePrepaid,excludeTicket)

        return  PaymentMethodsModel(listPaymentMethods)
    }


}
