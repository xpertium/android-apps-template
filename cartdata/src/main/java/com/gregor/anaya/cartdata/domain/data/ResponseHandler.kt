package com.gregor.anaya.cartdata.domain.data

import com.gregor.anaya.cartdata.data.common.Resource
import retrofit2.HttpException
import java.lang.Exception
import java.net.SocketTimeoutException

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1),
}
object Error{
    const val unauthorized = "Unauthorized"
    const val notFound = "Not found"
    const val timeOut = "Time Out"
    const val generic = "Something went wrong"
    const val imageInvalid = "Su imagen no cumple con las políticas de LaCausa"
}
object CodError{
    const val UNAUTHORIZED = 401
    const val NOT_FOUND = 404
    const val IMAGE_INVALID= 400
}

open class ResponseHandler {
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is HttpException -> Resource.error(
                getErrorMessage(e.code()),
                null, null, null
            )
            is SocketTimeoutException -> Resource.error(
                getErrorMessage(ErrorCodes.SocketTimeOut.code),
                null, null, null
            )
            else -> Resource.error(
                getErrorMessage(Int.MAX_VALUE),
                null, null, null
            )
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> Error.timeOut
            CodError.UNAUTHORIZED -> Error.unauthorized
            CodError.NOT_FOUND -> Error.notFound
            CodError.IMAGE_INVALID -> Error.imageInvalid
            else -> Error.generic
        }
    }

    fun handleSuccess(data: Unit?): Resource<Unit> {
        return Resource.success(data)
    }
}
