package com.gregor.anaya.cartdata.domain.data.server

import com.gregor.anaya.model.entity.response.mercadopago.ReferenceRequest
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceResponse
import retrofit2.http.*

interface UserService {

    @POST("checkout/preferences")
    suspend fun createPreference(
        @Query("access_token") accessToken: String,
        @Body items: ReferenceRequest
    ) : ReferenceResponse


}