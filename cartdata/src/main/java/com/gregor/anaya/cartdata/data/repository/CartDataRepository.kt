package com.gregor.anaya.cartdata.data.repository

import androidx.lifecycle.LiveData
import com.gregor.anaya.cartdata.data.common.Resource
import com.gregor.anaya.cartdata.data.datastore.PreferenceDataStore
import com.gregor.anaya.cartdata.data.room.CartRoom
import com.gregor.anaya.cartdata.data.room.dao.Cart
import com.gregor.anaya.cartdata.data.room.dao.CartDao
import com.gregor.anaya.cartdata.domain.repository.CartRepository
import com.gregor.anaya.model.entity.response.mercadopago.ReferenceResponse
import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer
import kotlinx.coroutines.CoroutineScope

class CartDataRepository(val data : PreferenceDataStore) : CartRepository{

    // TODO 4. SAVES CART PRODUCTS

    lateinit var cart : Cart
    private val cartDao: CartDao
    val listCart: LiveData<List<Cart>>

    init {
        val database = CartRoom.database
        cartDao = database!!.cartDao()
        listCart = cartDao.getAll()
    }


    override suspend fun getAll(): LiveData<List<Cart>> {
        return cartDao.getAll()
    }

    override suspend fun createPreference(items : List<ItemModel>, payer: Payer): Resource<ReferenceResponse> {
      return  data.createPreference(items, payer)
    }

    override suspend fun insert(vararg cart: Cart) {
                cartDao.insert(*cart)
    }

    override suspend fun delete(vararg cart: Cart) {
            cartDao.deleteOne(*cart)
        }

    override suspend fun deleteAll() {

            cartDao.delete()
        }

    override suspend fun update(vararg cart: Cart) {

            cartDao.update(*cart)
        }



}