package com.gregor.anaya.cartdata.data.room

import android.content.Context
import androidx.room.Room

class CartRoom {

    companion object {
        var database: CartDataBase? = null
    }


    fun initRoomCart(context: Context) {
        database = Room.databaseBuilder(
            context,
            CartDataBase::class.java,
            "cart-db")
            .build()
    }
}