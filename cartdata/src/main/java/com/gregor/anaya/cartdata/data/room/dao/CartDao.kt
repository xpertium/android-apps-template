package com.gregor.anaya.cartdata.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CartDao {

    @Query("SELECT * FROM cart")
    fun getAll() : LiveData<List<Cart>>

    @Insert
    fun insert(vararg cart: Cart)

    @Query("DELETE FROM cart")
    fun delete()

    @Delete
    fun deleteOne(vararg cart: Cart)

    @Update
    fun update(vararg cart: Cart)

}