package com.gregor.anaya.data.data.service

import com.google.gson.Gson
import com.gregor.anaya.data.data.api.ApiUrl
import com.gregor.anaya.data.data.datastore.DetailDataStore
import com.gregor.anaya.model.entity.response.detail.DetailResponse
import com.gregor.anaya.model.entity.response.error.ErrorResponse
import com.gregor.anaya.model.model.detail.DetailModel
import com.gregor.anaya.networking.NetworkingManager
import com.gregor.anaya.networking.model.NetworkingConfiguration
import com.gregor.anaya.util.data.ErrorUtil
import com.gregor.anaya.util.data.UrlUtil
import com.gregor.anaya.util.domain.Failure
import com.gregor.anaya.util.domain.ResultType
import com.gregor.anaya.model.mapper.detail.DetailMapper
import com.gregor.anaya.model.mapper.error.ErrorMapper

class DetailServiceDataStore : DetailDataStore {

    override suspend fun detail(id: Int): ResultType<DetailModel, Failure> {

        val gson = Gson()

        val networkingConfiguration = NetworkingConfiguration.NetworkingConfigurationBuilder()
            .endpoint(UrlUtil.replaceUrlParameter("movie_id", id, ApiUrl.URL_DETAIL))
            .config()

        return try {
            val result =
                NetworkingManager.with(networkingConfiguration)
                    .connect()
            if (result.isSuccessful) {
                val detailResponse = gson.fromJson(gson.toJson(result.body), DetailResponse::class.java)
                ResultType.Success(DetailMapper.transform(detailResponse))
            } else {
                val errorResponse: ErrorResponse = gson.fromJson(gson.toJson(result.error), ErrorResponse::class.java)
                ResultType.Error(Failure.ClientError(ErrorMapper.transform(errorResponse)))
            }
        } catch (t: Throwable) {
            ResultType.Error(ErrorUtil.errorHandler(t))
        }
    }
}