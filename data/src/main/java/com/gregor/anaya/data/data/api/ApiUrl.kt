package com.gregor.anaya.data.data.api

import com.gregor.anaya.util.BuildConfig


object ApiUrl {

    const val URL_DETAIL = "movie/{movie_id}".plus("?api_key=").plus(BuildConfig.API_KEY).plus("&language=en-US")
}