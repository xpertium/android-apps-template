package com.gregor.anaya.data.domain.usecase

import com.gregor.anaya.data.domain.repository.DetailRepository
import com.gregor.anaya.model.model.detail.DetailModel
import com.gregor.anaya.util.domain.Failure
import com.gregor.anaya.util.domain.ResultType

class DetailUseCase(private val detailRepository: DetailRepository) {

    suspend fun detail(id: Int): ResultType<DetailModel, Failure> =
        detailRepository.detail(id)
}