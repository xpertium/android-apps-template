package com.gregor.anaya.data.data.datastore

import com.gregor.anaya.model.model.detail.DetailModel
import com.gregor.anaya.util.domain.Failure
import com.gregor.anaya.util.domain.ResultType

interface DetailDataStore {

    suspend fun detail(id: Int): ResultType<DetailModel, Failure>
}