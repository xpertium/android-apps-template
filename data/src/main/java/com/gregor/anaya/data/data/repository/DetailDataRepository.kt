package com.gregor.anaya.data.data.repository

import com.gregor.anaya.data.data.service.DetailServiceDataStore
import com.gregor.anaya.data.domain.repository.DetailRepository
import com.gregor.anaya.model.model.detail.DetailModel
import com.gregor.anaya.util.domain.Failure
import com.gregor.anaya.util.domain.ResultType

class DetailDataRepository : DetailRepository {

    override suspend fun detail(id: Int): ResultType<DetailModel, Failure> {
        val detailServiceDataStore = DetailServiceDataStore()
        return detailServiceDataStore.detail(id)
    }
}