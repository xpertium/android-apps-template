package com.gregor.anaya.data.domain.repository

import com.gregor.anaya.model.model.detail.DetailModel
import com.gregor.anaya.util.domain.Failure
import com.gregor.anaya.util.domain.ResultType

interface DetailRepository {

    suspend fun detail(id: Int): ResultType<DetailModel, Failure>
}