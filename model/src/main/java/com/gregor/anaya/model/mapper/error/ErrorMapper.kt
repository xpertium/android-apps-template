package com.gregor.anaya.model.mapper.error

import com.gregor.anaya.model.entity.response.error.ErrorResponse
import com.gregor.anaya.model.model.error.ErrorModel

object ErrorMapper {

    fun transform(errorResponse: ErrorResponse): ErrorModel {

        val errorModel = ErrorModel()
        errorModel.statusCode = errorResponse.statusCode
        errorModel.statusMessage = errorResponse.statusMessage

        return errorModel
    }
}