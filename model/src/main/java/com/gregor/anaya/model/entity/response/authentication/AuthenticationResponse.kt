package com.gregor.anaya.model.entity.response.authentication

import com.google.gson.annotations.SerializedName

data class AuthenticationResponse(
    var success: Boolean = true,
    @SerializedName("expires_at")
    var expiresAt: String? = null,
    @SerializedName("request_token")
    var requestToken: String? = null
)