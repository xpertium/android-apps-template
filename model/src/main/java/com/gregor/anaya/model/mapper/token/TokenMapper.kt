package com.gregor.anaya.model.mapper.token

import com.gregor.anaya.model.entity.response.token.TokenResponse
import com.gregor.anaya.model.model.token.TokenModel

object TokenMapper {

    fun transform(tokenResponse: TokenResponse): TokenModel {

        val tokenModel = TokenModel()
        tokenModel.expiresAt = tokenResponse.expiresAt
        tokenModel.requestToken = tokenResponse.requestToken
        tokenModel.success = tokenResponse.success

        return tokenModel
    }
}