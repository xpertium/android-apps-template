package com.gregor.anaya.model.model.store

import java.io.Serializable

 class StoreModel: Serializable {
     var id : String? = ""
     var name : String? = ""
     var email : String? = ""
     var phone: String? = ""
     var logo: String? = ""
     var active: Boolean = false
     var type: String? = ""
     var position : Int = 0
}