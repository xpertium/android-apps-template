package com.gregor.anaya.model.model.error

data class ErrorModel(
    var statusCode: Int? = 0,
    var statusMessage: String? = null
)