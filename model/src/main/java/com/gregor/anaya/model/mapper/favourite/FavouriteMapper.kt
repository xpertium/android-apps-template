package com.gregor.anaya.model.mapper.favourite

import com.gregor.anaya.model.entity.response.favourite.FavouriteResponse
import com.gregor.anaya.model.model.favourite.FavouriteModel

object FavouriteMapper {

    private fun transform(favouriteModel: FavouriteModel): FavouriteResponse {

        val favouriteResponse = FavouriteResponse()

        favouriteResponse.id = favouriteModel.id
        favouriteResponse.voteCount = favouriteModel.voteCount
        favouriteResponse.isVideo = favouriteModel.isVideo
        favouriteResponse.voteAverage = favouriteModel.voteAverage
        favouriteResponse.title = favouriteModel.title
        favouriteResponse.popularity = favouriteModel.popularity
        favouriteResponse.posterPath = favouriteModel.posterPath
        favouriteResponse.originalLanguage = favouriteModel.originalLanguage
        favouriteResponse.originalTitle = favouriteModel.originalTitle
        favouriteResponse.backdropPath = favouriteModel.backdropPath
        favouriteResponse.isAdult = favouriteModel.isAdult
        favouriteResponse.overview = favouriteModel.overview
        favouriteResponse.releaseDate = favouriteModel.releaseDate

        return favouriteResponse
    }

    private fun transform(favouriteResponse: FavouriteResponse): FavouriteModel {

        val favouriteModel = FavouriteModel()

        favouriteModel.id = favouriteResponse.id
        favouriteModel.voteCount = favouriteResponse.voteCount
        favouriteModel.isVideo = favouriteResponse.isVideo
        favouriteModel.voteAverage = favouriteResponse.voteAverage
        favouriteModel.title = favouriteResponse.title
        favouriteModel.popularity = favouriteResponse.popularity
        favouriteModel.posterPath = favouriteResponse.posterPath
        favouriteModel.originalLanguage = favouriteResponse.originalLanguage
        favouriteModel.originalTitle = favouriteResponse.originalTitle
        favouriteModel.backdropPath = favouriteResponse.backdropPath
        favouriteModel.isAdult = favouriteResponse.isAdult
        favouriteModel.overview = favouriteResponse.overview
        favouriteModel.releaseDate = favouriteResponse.releaseDate

        return favouriteModel
    }

    fun transform(favouriteResponseList: List<FavouriteResponse>?): List<FavouriteModel> {

        val list = arrayListOf<FavouriteModel>()

        favouriteResponseList.let {
            favouriteResponseList?.map {
                list.add(FavouriteMapper.transform(it))
            }
        }

        return list
    }

    fun addFavourite(favouriteModel: FavouriteModel, list: List<FavouriteResponse>): List<FavouriteResponse> {

        val favouriteResponse: FavouriteResponse =
            transform(favouriteModel)
        val mutableList = list.toMutableList()

        mutableList.toMutableList().map {
            if (favouriteResponse.id == it.id) {
                mutableList.remove(it)
            }
        }

        mutableList.add(favouriteResponse)
        return mutableList
    }

    fun removeFavourite(favouriteModel: FavouriteModel, list: List<FavouriteResponse>): List<FavouriteResponse> {

        val favouriteResponse: FavouriteResponse =
            transform(favouriteModel)
        val mutableList = list.toMutableList()

        mutableList.toMutableList().map {
            if (favouriteResponse.id == it.id) {
                mutableList.remove(it)
            }
        }
        return mutableList
    }
}