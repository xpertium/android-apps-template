package com.gregor.anaya.model.model.mecadopago

data class ItemModel (
    val title : String,
    val description : String,
    val quantity : Int,
    val currency_id : String,
    val unit_price : Double
)

