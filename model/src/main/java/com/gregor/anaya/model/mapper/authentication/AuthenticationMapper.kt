package com.gregor.anaya.model.mapper.authentication

import com.gregor.anaya.model.entity.request.authentication.AuthenticationRequest
import com.gregor.anaya.model.entity.response.authentication.AuthenticationResponse
import com.gregor.anaya.model.model.authentication.AuthenticationRequestModel
import com.gregor.anaya.model.model.authentication.AuthenticationResponseModel

object AuthenticationMapper {

    fun transform(authenticationRequest: AuthenticationRequest): AuthenticationRequestModel {
        val authenticationRequestModel = AuthenticationRequestModel()
        authenticationRequestModel.username = authenticationRequest.username
        authenticationRequestModel.requestToken = authenticationRequest.requestToken
        authenticationRequestModel.pwd = authenticationRequest.pwd
        return authenticationRequestModel
    }

    fun transform(authenticationRequestModel: AuthenticationRequestModel): AuthenticationRequest {
        val authenticationRequest = AuthenticationRequest()
        authenticationRequest.username = authenticationRequestModel.username
        authenticationRequest.requestToken = authenticationRequestModel.requestToken
        authenticationRequest.pwd = authenticationRequestModel.pwd
        return authenticationRequest
    }

    fun transform(authenticationResponse: AuthenticationResponse): AuthenticationResponseModel {
        val authenticationResponseModel = AuthenticationResponseModel()
        authenticationResponseModel.expiresAt = authenticationResponse.expiresAt
        authenticationResponseModel.requestToken = authenticationResponse.requestToken
        authenticationResponseModel.success = authenticationResponse.success
        return authenticationResponseModel
    }

    fun transform(authenticationResponseModel: AuthenticationResponseModel): AuthenticationResponse {
        val authenticationResponse = AuthenticationResponse()
        authenticationResponse.expiresAt = authenticationResponseModel.expiresAt
        authenticationResponse.requestToken = authenticationResponseModel.requestToken
        authenticationResponse.success = authenticationResponseModel.success
        return authenticationResponse
    }
}