package com.gregor.anaya.model.model.detail

data class DetailModel(
    var id: Int = 0,
    var originalTitle: String? = null,
    var overview: String? = null,
    var posterPath: String? = null,
    var genres: List<GenresModel> = arrayListOf()
) {
    data class GenresModel(
        var id: Int = 0,
        var name: String? = null
    )
}