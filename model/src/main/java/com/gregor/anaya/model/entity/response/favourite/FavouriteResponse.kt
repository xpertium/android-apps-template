package com.gregor.anaya.model.entity.response.favourite

import com.google.gson.annotations.SerializedName

data class FavouriteResponse(
    var id: Int = 0,
    @SerializedName("vote_count")
    var voteCount: Int = 0,
    var isVideo: Boolean = false,
    @SerializedName("vote_average")
    var voteAverage: Double = 0.toDouble(),
    var title: String? = null,
    var popularity: Double = 0.toDouble(),
    @SerializedName("poster_path")
    var posterPath: String? = null,
    @SerializedName("original_language")
    var originalLanguage: String? = null,
    @SerializedName("original_title")
    var originalTitle: String? = null,
    @SerializedName("backdrop_path")
    var backdropPath: String? = null,
    var isAdult: Boolean = false,
    var overview: String? = null,
    @SerializedName("release_date")
    var releaseDate: String? = null
)