package com.gregor.anaya.model.entity.response.token

import com.google.gson.annotations.SerializedName

data class TokenResponse(
    var success: Boolean = true,
    @SerializedName("expires_at")
    var expiresAt: String? = null,
    @SerializedName("request_token")
    var requestToken: String? = null
)