package com.gregor.anaya.model.model.authentication

data class AuthenticationRequestModel(
    var username: String? = null,
    var pwd: String? = null,
    var requestToken: String? = null
)