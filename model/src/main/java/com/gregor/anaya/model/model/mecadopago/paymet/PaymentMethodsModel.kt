package com.gregor.anaya.model.model.mecadopago.paymet

import java.util.*

data class PaymentMethodsModel (
    val excluded_payment_types: List<ExcludePaymentTypeModel>

)