package com.gregor.anaya.model.mapper.movie

import com.gregor.anaya.model.entity.response.movie.MovieResponse
import com.gregor.anaya.model.model.favourite.FavouriteModel
import com.gregor.anaya.model.model.movie.MovieModel

object MovieMapper {

    private fun transform(movieResponse: MovieResponse.ResultResponse): MovieModel.ResultResponseModel {

        val movieModel: MovieModel.ResultResponseModel = MovieModel.ResultResponseModel()

        movieModel.id = movieResponse.id
        movieModel.voteCount = movieResponse.voteCount
        movieModel.isVideo = movieResponse.isVideo
        movieModel.voteAverage = movieResponse.voteAverage
        movieModel.title = movieResponse.title
        movieModel.popularity = movieResponse.popularity
        movieModel.posterPath = movieResponse.posterPath
        movieModel.originalLanguage = movieResponse.originalLanguage
        movieModel.originalTitle = movieResponse.originalTitle
        movieModel.backdropPath = movieResponse.backdropPath
        movieModel.isAdult = movieResponse.isAdult
        movieModel.overview = movieResponse.overview
        movieModel.releaseDate = movieResponse.releaseDate

        return movieModel
    }

    fun transformResponseToModelList(movieResponse: MovieResponse): MovieModel {

        val movieModel = MovieModel()
        movieModel.totalResults = movieResponse.totalResults
        movieModel.totalPages = movieResponse.totalPages
        movieModel.page = movieResponse.page

        val resultResponse = arrayListOf<MovieModel.ResultResponseModel>()

        movieResponse.resultResponse.let {
            movieResponse.resultResponse!!.map {
                resultResponse.add(MovieMapper.transform(it))
            }
        }

        movieModel.resultResponse = resultResponse

        return movieModel
    }

    fun validateFavourite(movieModel: MovieModel, favourites: List<FavouriteModel>): MovieModel {
        val mutableList = movieModel.resultResponse?.toMutableList()

        mutableList?.map {
            val movie = it
            favourites.map {
                movie.favourite = movie.id == it.id
            }
        }
        movieModel.resultResponse = mutableList
        return movieModel
    }

    fun transform(movieModel: MovieModel.ResultResponseModel): FavouriteModel {

        val favouriteModel = FavouriteModel()

        favouriteModel.id = movieModel.id
        favouriteModel.voteCount = movieModel.voteCount
        favouriteModel.isVideo = movieModel.isVideo
        favouriteModel.voteAverage = movieModel.voteAverage
        favouriteModel.title = movieModel.title
        favouriteModel.popularity = movieModel.popularity
        favouriteModel.posterPath = movieModel.posterPath
        favouriteModel.originalLanguage = movieModel.originalLanguage
        favouriteModel.originalTitle = movieModel.originalTitle
        favouriteModel.backdropPath = movieModel.backdropPath
        favouriteModel.isAdult = movieModel.isAdult
        favouriteModel.overview = movieModel.overview
        favouriteModel.releaseDate = movieModel.releaseDate

        return favouriteModel
    }
}
