package com.gregor.anaya.model.entity.response.mercadopago

import com.gregor.anaya.model.model.mecadopago.ItemModel
import com.gregor.anaya.model.model.mecadopago.Payer
import com.gregor.anaya.model.model.mecadopago.paymet.PaymentMethodsModel

data class ReferenceRequest(
    var items : List<ItemModel>,
    var payer : Payer,
    var payment_methods: PaymentMethodsModel
)