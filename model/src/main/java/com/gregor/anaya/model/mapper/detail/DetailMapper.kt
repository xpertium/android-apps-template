package com.gregor.anaya.model.mapper.detail

import com.gregor.anaya.model.entity.response.detail.DetailResponse
import com.gregor.anaya.model.model.detail.DetailModel

object DetailMapper {

    public fun transform(detailResponse: DetailResponse): DetailModel {

        val detailModel = DetailModel()

        detailModel.id = detailResponse.id
        detailModel.originalTitle = detailResponse.originalTitle
        detailModel.overview = detailResponse.overview
        detailModel.posterPath = detailResponse.posterPath

        val genresModel = arrayListOf<DetailModel.GenresModel>()

        detailResponse.genres.let {
            detailResponse.genres.map {
                genresModel.add(DetailMapper.transform(it))
            }
        }

        detailModel.genres = genresModel

        return detailModel
    }

    private fun transform(genresResponse: DetailResponse.GenresResponse): DetailModel.GenresModel {
        val genresModel = DetailModel.GenresModel()

        genresModel.id = genresResponse.id
        genresModel.name = genresResponse.name

        return genresModel
    }
}