package com.gregor.anaya.model.model.mecadopago.paymet

import java.util.*

data class ExcludePaymentTypeModel (
    val id: String
)