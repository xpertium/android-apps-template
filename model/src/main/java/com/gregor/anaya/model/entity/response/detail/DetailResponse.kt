package com.gregor.anaya.model.entity.response.detail

import com.google.gson.annotations.SerializedName

data class DetailResponse(
    var id: Int = 0,
    @SerializedName("original_title")
    var originalTitle: String? = null,
    @SerializedName("overview")
    var overview: String? = null,
    @SerializedName("poster_path")
    var posterPath: String? = null,
    var genres: List<GenresResponse>
) {
    data class GenresResponse(
        var id: Int = 0,
        var name: String? = null
    )
}