package com.gregor.anaya.model.model.product

import com.google.gson.Gson
import java.io.Serializable
import java.util.ArrayList

class ProductModel : Serializable {
    var id : String? = null
    var name : String? = null
    var isOutfit: Boolean = false
    var description: String? = null // descripcion breve del modelo
    var type: Int = 0 // tipo de prenda niña o adultas  :  1 = niña, 2 = mujer (Genero)
    var typeClothing: Int = 0 // tipo de vestimenta, blusas, vestidos,chompas, etc ...
    var idBrand : String = ""
    /**
     * blusas = 1
     * tops = 2
     * polos = 3
     * abrigos = 4[subtype]
     * pantalones = 5
     * short = 6
     * faldas = 7
     * vestidos = 8
     *
     */
    var retailPrice: Double = 0.toDouble() //precio al por menor
    var wholesalePrice: Double = 0.toDouble() //precio al por mayor
    var promotion: Boolean = false// SI TIENEN PROMOCION valores  = verdadero o falso
    var discount: Int = 0 // descuento en porcentaje
    var imageList: ArrayList<String>? = null
    var size: ArrayList<String>? = null //Talla
    var colorList: ArrayList<String>? = null
    var priority: Int = 0
    var quantity: Int = 0
    var isWishList : Boolean = false
    var isCart : Boolean = false




    fun toJson(value: ProductModel): String {
        val gson = Gson()
        return gson.toJson(value)
    }

    fun toJsonList(value: List<ProductModel>): String {
        val gson = Gson()
        return gson.toJson(value)
    }


    fun fromJson(value: String): ProductModel {
        val gson = Gson()
        return gson.fromJson(value,ProductModel::class.java)
    }

    fun fromJsonList(value: List<String>): List<ProductModel> {
        val gson = Gson()
        var list = mutableListOf<ProductModel>()
        for (item in value){
          val product =  gson.fromJson(item, ProductModel::class.java)
            list.add(product)
        }

        return list
    }
}