package com.gregor.anaya.model.model.mecadopago

data class Payer (
    val name : String,
    val surname: String,
    val email: String,
    val data_created : String,
    val phone : String
)
