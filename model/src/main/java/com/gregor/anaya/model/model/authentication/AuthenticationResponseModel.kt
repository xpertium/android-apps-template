package com.gregor.anaya.model.model.authentication

data class AuthenticationResponseModel(
    var success: Boolean = true,
    var expiresAt: String? = null,
    var requestToken: String? = null
)