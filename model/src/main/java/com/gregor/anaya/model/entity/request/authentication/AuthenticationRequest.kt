package com.gregor.anaya.model.entity.request.authentication

import com.google.gson.annotations.SerializedName

data class AuthenticationRequest(
    var username: String? = null,
    @SerializedName("password")
    var pwd: String? = null,
    @SerializedName("request_token")
    var requestToken: String? = null
)