package com.gregor.anaya.model.entity.response.mercadopago


data class ReferenceResponse (
    val id : String
)