package com.gregor.anaya.model.model.token

data class TokenModel(
    var success: Boolean = true,
    var expiresAt: String? = null,
    var requestToken: String? = null
)